unit Main;

interface

uses
	Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, System.Types,
	Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
	DataTypes, ImageButton, Vcl.ExtCtrls;

type
	TFMain = class(TForm)
		buttFontL: TLabel;
		Shape1: TShape;
		titleL: TLabel;
		subtitleL: TLabel;
    buttFontL2: TLabel;
		procedure FormCreate(Sender: TObject);
	private
		procedure CloseApplication(Sender: TObject);
		procedure ViewDatabase(Sender: TObject);
		procedure UpdateDatabase(Sender: TObject);
		procedure AboutApplication(Sender: TObject);
	public
		{ Public declarations }
	end;

var
	FMain: TFMain;
	UpdateB, ViewB, AboutB, CloseB: TImageButton;

implementation

{$R *.dfm}


uses About, Updater, Loading, Viewer;

procedure TFMain.FormCreate(Sender: TObject);
begin
	//
	with FormatSettings do
		begin
			DecimalSeparator := '.';
			ThousandSeparator := ' '
		end;
	//
	FMain.Color := FormColor;
	UpdateB := TImageButton.CreateImageButton(FMain, 'Update data', buttFontL.Font, clBlack, ControlColor, ControlMOColor, Rect(40, 80, 240, 60));
	UpdateB.OnClick := UpdateDatabase;
	ViewB := TImageButton.CreateImageButton(FMain, 'View database', buttFontL2.Font, clBlack, ControlColor, ControlMOColor, Rect(40, 145, 240, 60));
	ViewB.OnClick := ViewDatabase;
	AboutB := TImageButton.CreateImageButton(FMain, 'About the app', buttFontL.Font, clBlack, ControlColor, ControlMOColor, Rect(40, 210, 240, 60));
	AboutB.OnClick := AboutApplication;
	CloseB := TImageButton.CreateImageButton(FMain, 'Exit', buttFontL.Font, clBlack, ControlColor, ControlMOColor, Rect(40, 275, 240, 60));
	CloseB.OnClick := CloseApplication;
	Shape1.brush.Style := bsClear;
	Shape1.SetBounds(4, 4, FMain.Width - 8, FMain.Height - 8)
end;

procedure TFMain.CloseApplication(Sender: TObject);
begin
	FMain.Close
end;

procedure TFMain.ViewDatabase(Sender: TObject);
begin
	FLoading.ShowModal
end;

procedure TFMain.UpdateDatabase(Sender: TObject);
begin
	MessageDlg('Temporarily blocked. Sorry ;)', mtInformation, [mbOk], 0)
	// FUpdater.ShowModal
end;

procedure TFMain.AboutApplication(Sender: TObject);
begin
	FAbout.ShowModal
end;

end.
