unit DepartureTime;

interface

uses
	Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
	Dialogs;

type
	TDepartureTime = class
	public
		Hour: Byte;
		Minute: Byte;
		constructor Create(H, M: Byte); overload;
		constructor Create(DepTime: TDepartureTime); overload;
		function Time: string;
		function ToString: string; override;
	end;

function CompareDepartureTimes(DT1, DT2: TDepartureTime): Integer;

implementation

// Who is greatest? ... -1 for DT1 ... 0 for equal ... 1 for DT2
function CompareDepartureTimes(DT1, DT2: TDepartureTime): Integer;
begin
	if DT1.Hour > DT2.Hour then
		Result := - 1
	else if DT1.Hour < DT2.Hour then
		Result := 1
	else
		begin
			if DT1.Minute > DT2.Minute then
				Result := - 1
			else if DT1.Minute < DT2.Minute then
				Result := 1
			else
				Result := 0
		end
end;

//

constructor TDepartureTime.Create(H, M: Byte);
begin
	Hour := H;
	Minute := M
end;

constructor TDepartureTime.Create(DepTime: TDepartureTime);
begin
	Hour := DepTime.Hour;
	Minute := DepTime.Minute
end;

function TDepartureTime.Time: string;
begin
	Result := Format('%d:%.2d', [Hour, Minute])
end;

function TDepartureTime.ToString: string;
begin
	Result := Time
end;

end.
