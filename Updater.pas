unit Updater;

interface

uses
	Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
	Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls,
	ImageButton, DataTypes, XML.VerySimple;

type
	TFUpdater = class(TForm)
		Shape1: TShape;
		titleL: TLabel;
		subtitleL: TLabel;
		RE: TRichEdit;
		Label1: TLabel;
		Label2: TLabel;
		Label3: TLabel;
		Label0: TLabel;
		TestT: TTimer;
		Label4: TLabel;
		Label5: TLabel;
		Label6: TLabel;
		LineIndexT: TTimer;
		StationListT: TTimer;
		TimetableT: TTimer;
		FinishT: TTimer;
		Label7: TLabel;
		ch1: TCheckBox;
		ch2: TCheckBox;
		procedure FormCreate(Sender: TObject);
		procedure FormShow(Sender: TObject);
		procedure TestTTimer(Sender: TObject);
		procedure LineIndexTTimer(Sender: TObject);
		procedure StationListTTimer(Sender: TObject);
		procedure TimetableTTimer(Sender: TObject);
    procedure FinishTTimer(Sender: TObject);
	private
		procedure CloseForm(Sender: TObject);
		procedure StopDownload(Sender: TObject);
		function DownloadWebpage(Address, DestinationPath: string): boolean;
		function DownloadWebpageWithIndex(Address, DestinationPath: string; Index1N, Total: Word): boolean;
		procedure DecodeLineIndexFile(FilePath: string);
		procedure DecodeStationListFile(FilePath, Line, Direction: string);
		procedure DecodeTimetable(FilePath: string; StationNode: TXMLNode);
	public
		procedure AddLog(Priority: Byte; Text: string);
	end;

var
	FUpdater: TFUpdater;
	StopB, CloseB: TImageButton;

	XML: TXMLVerySimple;

	TotalItems, CurrentIndex, CurrentIndex2: Integer;
	TotalDecoded: Word;
	Timetables: TXMLNodeList;

implementation

{$R *.dfm}


uses Main, FunctiiBaza, DateUtils;

// FORM-RELATED

procedure TFUpdater.FormCreate(Sender: TObject);
begin
	FUpdater.Color := FormColor;
	StopB := TImageButton.CreateImageButton(FUpdater, 'Stop download', FMain.buttFontL.Font, clBlack, ControlColor, ControlMOColor,
		Rect(FUpdater.Width div 2 - ButtonWidth - 5, RE.Top + RE.Height + 20, ButtonWidth, ButtonHeight));
	StopB.OnClick := StopDownload;
	CloseB := TImageButton.CreateImageButton(FUpdater, 'Close', FMain.buttFontL.Font, clBlack, ControlColor, ControlMOColor,
		Rect(FUpdater.Width div 2 + 5, RE.Top + RE.Height + 20, ButtonWidth, ButtonHeight));
	CloseB.OnClick := CloseForm;
	Shape1.SetBounds(4, 4, FUpdater.Width - 8, FUpdater.Height - 8)
end;

procedure TFUpdater.StopDownload(Sender: TObject);
begin
	TestT.Enabled := False;
	LineIndexT.Enabled := False;
	StationListT.Enabled := False;
	TimetableT.Enabled := False;
	FinishT.Enabled := False;
	AddLog(0, HaltDownloadText + NL)
end;

procedure TFUpdater.CloseForm(Sender: TObject);
begin
	StopDownload(Sender);
	FUpdater.Close
end;

procedure TFUpdater.FormShow(Sender: TObject);
begin
	RE.Clear;
	AddLog(1, 'Preparing update process...' + NL);
	TestT.Enabled := True
end;

// GENERAL

procedure TFUpdater.AddLog(Priority: Byte; Text: string);
var Comp: TComponent;
begin
	RE.SelStart := Length(RE.Text);
	SendMessage(RE.Handle, EM_SCROLLCARET, 0, 0);
	Comp := FUpdater.FindComponent('Label' + IntToStr(Priority));
	if Comp <> nil then
		RE.SelAttributes.Assign(TLabel(Comp).Font)
	else
		RE.SelAttributes.Assign(Label3.Font);
	RE.SelText := Text;
	if ch2.Checked then
		SendMessage(RE.Handle, WM_VSCROLL, SB_BOTTOM, 0);
	if (Priority = 0) and (ch1.Checked) and (Text <> HaltDownloadText + NL) then
		StopDownload(FUpdater)
end;

function TFUpdater.DownloadWebpage(Address, DestinationPath: string): boolean;
begin
	AddLog(3, 'Downloading ''');
	AddLog(4, Address);
	AddLog(3, ''' ... ');
	Result := FunctiiBaza.download(Address, DestinationPath);
	if Result then
		AddLog(5, 'OK' + NL)
	else
		AddLog(6, 'Failed' + NL)
end;

function TFUpdater.DownloadWebpageWithIndex(Address, DestinationPath: string; Index1N, Total: Word): boolean;
begin
	AddLog(3, 'Downloading ');
	AddLog(7, Format('page %d / %d', [Index1N, Total]));
	AddLog(3, ' from ''');
	AddLog(4, Address);
	AddLog(3, ''' ... ');
	Result := FunctiiBaza.download(Address, DestinationPath);
	if Result then
		AddLog(5, 'OK' + NL)
	else
		AddLog(6, 'Failed' + NL)
end;

// TEST INTERNET

procedure TFUpdater.TestTTimer(Sender: TObject);
begin
	TestT.Enabled := False;
	AddLog(1, 'Testing internet connection...' + NL);
	if not DownloadWebpage(TestWebpage, 'temp.html') then
		begin
			AddLog(0, 'RAT Brasov''s website is currently unreachable. The operation has been aborted.' + NL);
			Exit
		end;
	AddLog(2, 'The connection appears to be working properly.' + NL);
	XML := TXMLVerySimple.Create;
	XML.Root.NodeName := 'TempDatabase';
	XML.Root.SetAttribute('modified', FormatDateTime('d-mmmm-yyyy, h:nn:ss', now));
	AddLog(1, 'Downloading line index webpages...' + NL);
	CurrentIndex := 1;
	LineIndexT.Enabled := True
end;

// LINE INDEX WEBPAGE DOWNLOAD

procedure TFUpdater.DecodeLineIndexFile(FilePath: string);
var SL: TStringList; X: TXMLVerySimple; List: TXMLNodeList; Phase, S, FilePath2: string; P, NDecoded: Integer; C: Char;
begin
	try
		// Clean up the HTML
		Phase := 'initialization';
		SL := TStringList.Create;
		SL.LoadFromFile(FilePath);
		Phase := 'cropping the "table" (part 1)';
		while Pos('Lista TRASEE', SL[0]) = 0 do
			SL.Delete(0);
		S := SL[0];
		Delete(S, 1, Pos('<table', S) - 1);
		SL[0] := S;
		Phase := 'cropping the "table" (part 2)';
		P := 0;
		repeat
			Inc(P);
			if P > 1023 then
				Break;
		until Pos('</table>', SL[P]) <> 0;
		S := SL[P];
		Delete(S, Pos('</table>', S) + 8, Length(S));
		SL[P] := S;
		Inc(P);
		while SL.Count > P do
			SL.Delete(P);
		Phase := 'tag cleanup';
		for P := 0 to SL.Count - 1 do
			begin
				S := SL[P];
				S := StringReplace(S, Chr(39), '', [rfReplaceAll]);
				S := StringReplace(S, '<', NL + '<', [rfReplaceAll]);
				if Copy(S, 1, 2) = NL then
					Delete(S, 1, 2);
				S := StringReplace(S, 'face=\', 'face="', [rfReplaceAll]);
				S := StringReplace(S, 'color="#FFFF00"', 'color="#FFFF00" /', [rfReplaceAll]);
				S := StringReplace(S, 'href=/', 'href="/', [rfReplaceAll]);
				for C := '0' to '9' do
					begin
						S := StringReplace(S, C + '>', C + '">', [rfReplaceAll]);
						S := StringReplace(S, '=' + C, '="' + C, [rfReplaceAll]);
						S := StringReplace(S, 'id="' + C, 'id=' + C, [rfReplaceAll]);
						S := StringReplace(S, C + ' ', C + '" ', [rfReplaceAll]);
					end;
				S := StringReplace(S, 'Uzina 2"', 'Uzina 2', [rfReplaceAll]);
				SL[P] := S
			end;
		P := 0;
		while P < SL.Count do
			begin
				if (SL[P] = '') or (SL[P] = ' ') then
					begin
						SL.Delete(P);
						Inc(P, - 1)
					end;
				Inc(P)
			end;
		SL.SaveToFile(FilePath);
		SL.Free;
		// Try to load as XML
		Phase := 'loading as XML';
		FilePath2 := StringReplace(FilePath, '.html', '.xml', [rfReplaceAll]);
		X := TXMLVerySimple.Create;
		X.LoadFromFile(FilePath);
		X.SaveToFile(FilePath2);
		// Get line names
		Phase := 'parsing the XML';
		NDecoded := 0;
		List := X.Root.FindNodes('tr');
		for P := 0 to List.Count - 1 do
			begin
				Phase := 'parsing the XML, P=' + IntToStr(P);
				XML.Root.AddChild('line');
				with XML.Root.ChildNodes.Last do
					begin
						SetAttribute('name', AnsiLowerCase(List[P].ChildNodes[1].ChildNodes[0].Text));
						SetAttribute('type', AnsiLowerCase(List[P].ChildNodes[2].Text));
						SetAttribute('routeDescription', FormatRouteDescription(List[P].ChildNodes[3].ChildNodes[0].Text));
						SetAttribute('length', AnsiLowerCase(List[P].ChildNodes[4].ChildNodes[0].Text));
						Inc(NDecoded)
					end
			end;
		X.SaveToFile(FilePath2);
		X.Free;
		AddLog(3, 'Decoded ');
		AddLog(5, IntToStr(NDecoded) + ' line(s)');
		AddLog(3, ' from file ');
		AddLog(4, FilePath + NL);
	except
		on E: Exception do
			AddLog(0, Format('DecodeLineIndexFile[%s] ERROR (phase "%s")%s%s: %s%s', [FilePath, Phase, NL, E.ClassName, E.Message, NL]))
	end
end;

procedure TFUpdater.LineIndexTTimer(Sender: TObject);
begin
	LineIndexT.Enabled := False;
	if DownloadWebpage(LineIndexWebpage[CurrentIndex], LineIndexTempFiles[CurrentIndex]) then
		DecodeLineIndexFile(LineIndexTempFiles[CurrentIndex]);
	Inc(CurrentIndex);
	if CurrentIndex <= High(LineIndexWebpage) then
		LineIndexT.Enabled := True
	else
		begin
			XML.SaveToFile('db1.xml');
			AddLog(2, 'Decoded a total of ' + IntToStr(XML.Root.ChildNodes.Count) + ' line(s)' + NL);
			AddLog(1, 'Downloading station list webpages...' + NL);
			TotalItems := XML.Root.ChildNodes.Count;
			CurrentIndex := 0;
			TotalDecoded := 0;
			StationListT.Enabled := True
		end
end;

// STATION LIST WEBPAGE DOWNLOAD

procedure TFUpdater.DecodeStationListFile(FilePath, Line, Direction: string);
var SL: TStringList; X: TXMLVerySimple; List: TXMLNodeList; Phase, S, FilePath2: string; P, NDecoded: Integer;
begin
	try
		// Clean up the HTML
		Phase := 'initialization';
		SL := TStringList.Create;
		SL.LoadFromFile(FilePath);
		Phase := 'cropping the "div" tag (part 1)';
		while Pos('id="glue_file"', SL[0]) = 0 do
			SL.Delete(0);
		S := SL[0];
		Delete(S, 1, Pos('<div', S) - 1);
		SL[0] := S;
		Phase := 'cropping the "div" tag (part 2)';
		P := 0;
		repeat
			Inc(P);
			if P > 1023 then
				Break;
		until Pos('</body>', SL[P]) <> 0;
		Inc(P, - 1);
		S := SL[P];
		Delete(S, Pos('</div>', S) + 6, Length(S));
		SL[P] := S;
		Inc(P);
		while SL.Count > P do
			SL.Delete(P);
		SL.SaveToFile(FilePath);
		SL.Free;
		// Try to load as XML
		Phase := 'loading as XML';
		FilePath2 := StringReplace(FilePath, '.html', '.xml', [rfReplaceAll]);
		X := TXMLVerySimple.Create;
		X.LoadFromFile(FilePath);
		X.SaveToFile(FilePath2);
		// Get line names
		Phase := 'parsing the XML';
		NDecoded := 0;
		List := X.Root.FindNodes('div');
		for P := 0 to List.Count - 1 do
			begin
				Phase := 'parsing the XML, P=' + IntToStr(P);
				XML.Root.Find('line', 'name', Line).AddChild('station');
				with XML.Root.Find('line', 'name', Line).ChildNodes.Last do
					begin
						SetAttribute('href', AnsiLowerCase(List[P].ChildNodes[0].ChildNodes[0].Attribute['href']));
						SetAttribute('name', FormatRouteDescription(List[P].ChildNodes[0].ChildNodes[0].ChildNodes[0].Text));
						if Pos('..', Attribute['href']) <> 0 then
							Attribute['href'] := ''
						else
							Inc(NDecoded)
					end
			end;
		X.SaveToFile(FilePath2);
		X.Free;
		AddLog(3, 'Decoded ');
		AddLog(5, IntToStr(NDecoded) + ' station(s)');
		Inc(TotalDecoded, NDecoded);
		AddLog(3, ' from file ');
		AddLog(4, FilePath + NL);
	except
		on E: Exception do
			AddLog(0, Format('DecodeStationListFile[%s (%s-%s)] ERROR (phase "%s")%s%s: %s%s', [FilePath, Line, Direction, Phase, NL, E.ClassName, E.Message, NL]))
	end
end;

procedure TFUpdater.StationListTTimer(Sender: TObject);
var D, K: Word;
begin
	StationListT.Enabled := False;
	if CurrentIndex < TotalItems then
		begin
			for D := Low(LineDirections) to High(LineDirections) do
				if DownloadWebpageWithIndex(GetStationListWebpage(XML.Root.ChildNodes[CurrentIndex].Attribute['name'], LineDirections[D]),
					'temp.html', CurrentIndex * 2 + D + 1, TotalItems * 2) then
					DecodeStationListFile('temp.html', XML.Root.ChildNodes[CurrentIndex].Attribute['name'], LineDirections[D]);
			Inc(CurrentIndex);
			StationListT.Enabled := True
		end
	else
		begin
			XML.SaveToFile('db2.xml');
			AddLog(2, 'Decoded a total of ' + IntToStr(TotalDecoded) + ' stops(s)' + NL);
			Timetables := TXMLNodeList.Create;
			for D := 0 to XML.Root.ChildNodes.Count - 1 do
				if XML.Root.ChildNodes[D].ChildNodes.Count > 0 then
					for K := 0 to XML.Root.ChildNodes[D].ChildNodes.Count - 1 do
						if XML.Root.ChildNodes[D].ChildNodes[K].Attribute['href'] <> '' then
							Timetables.Add(XML.Root.ChildNodes[D].ChildNodes[K]);
			AddLog(1, 'Downloading timetable webpages...' + NL);
			TotalItems := Timetables.Count;
			CurrentIndex := 0;
			TotalDecoded := 0;
			TimetableT.Enabled := True
		end
end;

// TIMETABLE WEBPAGE DOWNLOAD

procedure TFUpdater.DecodeTimetable(FilePath: string; StationNode: TXMLNode);
var SL, SL2: TStringList; SLL: TList; X: TXMLVerySimple; TTNode: TXMLNode; List: TXMLNodeList;
	Phase, S, FilePath2, ListPText: string; P, K, K2, NDecoded: Integer;
begin
	try
		// Clean up the HTML
		Phase := 'initialization';
		SL := TStringList.Create;
		SL.LoadFromFile(FilePath);
		Phase := 'cropping to "tabel" (part 1)';
		while Pos('id="tabel', SL[0]) = 0 do
			SL.Delete(0);
		S := SL[0];
		Delete(S, 1, Pos('<div', S) - 1);
		SL[0] := S;
		Phase := 'cropping to "tabel" (part 2)';
		P := 0;
		repeat
			Inc(P);
			if P > 1023 then
				Break;
		until Pos('</body>', SL[P]) <> 0;
		Inc(P, - 1);
		S := SL[P];
		Delete(S, Pos('</div>', S) + 6, Length(S));
		SL[P] := S;
		Inc(P);
		while SL.Count > P do
			SL.Delete(P);
		P := SL.Count - 1;
		while (P > 0) and (Pos('id="dizabilitati100', SL[P]) = 0) do
			Inc(P, - 1);
		if P <> 0 then
			while SL.Count > P do
				SL.Delete(P);
		Phase := 'tag cleanup';
		for P := 0 to SL.Count - 1 do
			begin
				S := SL[P];
				S := StringReplace(S, '&#194', 'A', [rfReplaceAll]);
				S := StringReplace(S, '&#195', 'A', [rfReplaceAll]);
				S := StringReplace(S, '&nbsp;', ' ', [rfReplaceAll]);
				S := StringReplace(S, '&#195', 'A', [rfReplaceAll]);
				while Pos('  ', S) <> 0 do
					S := StringReplace(S, '  ', ' ', [rfReplaceAll]);
				SL[P] := S
			end;
		P := 0;
		while P < SL.Count do
			begin
				if (SL[P] = '') or (SL[P] = ' ') then
					begin
						SL.Delete(P);
						Inc(P, - 1)
					end;
				Inc(P)
			end;
		SL.SaveToFile(FilePath);
		//
		Phase := 'saving "table" areas as separate files';
		SLL := TList.Create;
		SL2 := TStringList.Create;
		SL2.Add(SL[0]);
		SL.Delete(0);
		while SL.Count > 0 do
			begin
				if Pos('id="tabel', SL[0]) <> 0 then
					begin
						SLL.Add(SL2);
						SL2 := TStringList.Create
					end;
				SL2.Add(SL[0]);
				SL.Delete(0)
			end;
		SLL.Add(SL2);
		SL.Free;
		// Try to load each things as XML
		for K := 0 to SLL.Count - 1 do
			begin
				Phase := 'loading as XML, K=' + IntToStr(K);
				FilePath2 := 'tempK.xml';
				SL := TStringList(SLL[K]);
				SL.SaveToFile(FilePath2);
				MakeEvenNumberOfOpenClosedTags(FilePath2, 'div');
				SL.LoadFromFile(FilePath2);
				X := TXMLVerySimple.Create;
				X.LoadFromFile(FilePath2);
				X.SaveToFile(FilePath2);
				//
				Phase := 'parsing the XML, K=' + IntToStr(K);
				NDecoded := 0;
				List := X.Root.FindNodes('div');
				for P := 0 to List.Count - 1 do
					begin
						Phase := 'parsing the XML, K=' + IntToStr(K) + ', P=' + IntToStr(P);
						if List[P].HasAttribute('id') then
							begin
								ListPText := StringReplace(List[P].Text, ' ', '', [rfReplaceAll]);
								if StrToCase(List[P].Attribute['id'], ['class_title100', 'class_title101', 'web_class_title']) <> - 1 then
									begin
										StationNode.AddChild('timetable');
										TTNode := StationNode.ChildNodes.Last;
										TTNode.SetAttribute('days', FormatDays(ListPText))
									end
								else
									if StrToCase(List[P].Attribute['id'], ['class_hours100', 'class_hours101', 'web_class_hours']) <> - 1 then
									begin
										if IsInteger(ListPText) then
											begin
												if TTNode = nil then
													AddLog(0, 'TTNode is nil... there''s probably a problem with the order of things in the timetable HTML.' + NL);
												TTNode.AddChild('clock');
												TTNode.ChildNodes.Last.SetAttribute('hour', ListPText);
												TTNode.ChildNodes.Last.SetAttribute('minutes', '')
											end
									end
								else if List[P].ChildNodes.Count > 0 then
									begin
										for K2 := 0 to List[P].ChildNodes.Count - 1 do
											begin
												ListPText := StringReplace(List[P].ChildNodes[K2].Text, ' ', '', [rfReplaceAll]);
												if IsInteger(ListPText) then
													begin
														TTNode.ChildNodes.Last.SetAttribute('minutes', TTNode.ChildNodes.Last.Attribute['minutes'] + ListPText + MinuteListDelimiter);
														Inc(NDecoded)
													end
											end
									end
							end
					end;
				X.Free;
				XML.SaveToFile('db2.xml');
			end;
	except
		on E: Exception do
			AddLog(0, Format('DecodeTimetable[%s (node.name=%s)] ERROR (phase "%s")%s%s: %s%s',
				[FilePath, StationNode.Attribute['name'], Phase, NL, E.ClassName, E.Message, NL]))
	end
end;

procedure TFUpdater.TimetableTTimer(Sender: TObject);
begin
	TimetableT.Enabled := False;
	if CurrentIndex < TotalItems then
		begin
			if DownloadWebpageWithIndex(
				GetTimetableWebpage(
				Timetables[CurrentIndex].Parent.Attribute['name'],
				GetDirectionFromHREF(Timetables[CurrentIndex].Attribute['href']),
				Timetables[CurrentIndex].Attribute['href']),
				'temp.html', CurrentIndex + 1, TotalItems
				) then
				DecodeTimetable('temp.html', Timetables[CurrentIndex]);
			Inc(CurrentIndex);
			TimetableT.Enabled := True
		end
	else
		begin
			AddLog(1, 'Finished downloading the timetables' + NL)
		end
end;

procedure TFUpdater.FinishTTimer(Sender: TObject);
begin
	//
  XML.Free
end;

end.
