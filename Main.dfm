object FMain: TFMain
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'RAT Brasov Database Menu'
  ClientHeight = 370
  ClientWidth = 320
  Color = 12510427
  Constraints.MaxHeight = 370
  Constraints.MaxWidth = 320
  Constraints.MinHeight = 370
  Constraints.MinWidth = 320
  Font.Charset = EASTEUROPE_CHARSET
  Font.Color = clBlack
  Font.Height = -19
  Font.Name = 'Segoe UI'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  ScreenSnap = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 25
  object buttFontL: TLabel
    Left = 112
    Top = 208
    Width = 80
    Height = 25
    Caption = 'buttFontL'
    Visible = False
  end
  object Shape1: TShape
    Left = 32
    Top = 74
    Width = 65
    Height = 65
    Brush.Color = 13546712
    Pen.Width = 2
  end
  object titleL: TLabel
    Left = 24
    Top = 16
    Width = 122
    Height = 31
    Caption = 'RAT Brasov'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clBlack
    Font.Height = -23
    Font.Name = 'Segoe UI'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object subtitleL: TLabel
    Left = 24
    Top = 41
    Width = 86
    Height = 17
    Caption = 'Huehuehuehue'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = 3426627
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object buttFontL2: TLabel
    Left = 112
    Top = 239
    Width = 95
    Height = 25
    Caption = 'buttFontL2'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clBlack
    Font.Height = -19
    Font.Name = 'Segoe UI Semibold'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
end
