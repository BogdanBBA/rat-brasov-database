object FViewerSettings: TFViewerSettings
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'Settings'
  ClientHeight = 323
  ClientWidth = 423
  Color = 12510427
  Font.Charset = EASTEUROPE_CHARSET
  Font.Color = clBlack
  Font.Height = -16
  Font.Name = 'Segoe UI'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  ScreenSnap = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 21
  object Shape1: TShape
    Left = 40
    Top = 8
    Width = 65
    Height = 65
    Brush.Style = bsClear
    Pen.Width = 2
  end
  object titleL: TLabel
    Left = 24
    Top = 16
    Width = 150
    Height = 30
    Caption = 'Viewer settings'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clBlack
    Font.Height = -21
    Font.Name = 'Segoe UI'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object subtitleL: TLabel
    Left = 24
    Top = 41
    Width = 64
    Height = 15
    Caption = 'Yes, we can!'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = 3426627
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Shape2: TShape
    Left = 136
    Top = 239
    Width = 169
    Height = 57
    Visible = False
  end
  object ch1: TCheckBox
    Left = 104
    Top = 79
    Width = 225
    Height = 17
    Caption = 'Show title image'
    Enabled = False
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clBlack
    Font.Height = -15
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object ch2: TCheckBox
    Left = 104
    Top = 102
    Width = 225
    Height = 17
    Caption = 'Show hour column'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clBlack
    Font.Height = -15
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
  end
  object ch3: TCheckBox
    Left = 104
    Top = 125
    Width = 225
    Height = 17
    Caption = 'Show line for each departure'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clBlack
    Font.Height = -15
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
  end
  object ch4: TCheckBox
    Left = 104
    Top = 148
    Width = 225
    Height = 17
    Caption = 'Show hour for each departure'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clBlack
    Font.Height = -15
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
  end
  object ch5: TCheckBox
    Left = 104
    Top = 171
    Width = 225
    Height = 17
    Caption = 'Use current date and time'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clBlack
    Font.Height = -15
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    OnClick = ch5Click
  end
  object dtp1: TDateTimePicker
    Left = 124
    Top = 194
    Width = 125
    Height = 25
    Date = 41694.417071469910000000
    Time = 41694.417071469910000000
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
  end
  object dtp2: TDateTimePicker
    Left = 255
    Top = 194
    Width = 90
    Height = 25
    Date = 41694.417071469910000000
    Time = 41694.417071469910000000
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    Kind = dtkTime
    ParentFont = False
    TabOrder = 6
  end
end
