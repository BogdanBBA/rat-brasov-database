unit Timetable;

interface

uses
	Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
	Dialogs, Generics.Collections, DepartureTime;

type
	TTimetable = class
	private
		theDescription: string;
	public
		Days: array [1 .. 7] of boolean;
		Departures: TList<TDepartureTime>;
		property Description: string read theDescription write theDescription;
		constructor Create(D7: array of boolean);
		destructor Free;
		function TotalDepartures: Integer;
		function ToString: string; override;
	end;

implementation

uses DataTypes, FunctiiBaza;

constructor TTimetable.Create(D7: array of boolean);
var i: Word;
begin
	for i := 1 to 7 do
		Days[i] := False;
	if Length(D7) > 0 then
		for i := 0 to Min(6, High(D7)) do
			Days[i + 1] := D7[i];
	Departures := TList<TDepartureTime>.Create
end;

destructor TTimetable.Free;
begin
	FreeAndNil(Departures)
end;

function TTimetable.TotalDepartures: Integer;
begin
	Result := Departures.Count
end;

function TTimetable.ToString: string;
var i: Word; S: string;
begin
	S := Format('Timetable(%d', [Integer(Days[1])]);
	for i := 2 to 7 do
		S := Format('%s, %d', [S, Integer(Days[i])]);
	S := S + ')' + NL;
	if Departures.Count > 0 then
		begin
			S := Format('%s .... ', [S]);
			for i := 0 to Departures.Count - 1 do
				S := Format('%s%s; ', [S, Departures[i].ToString])
		end;
	Result := S
end;

end.
