object FViewer: TFViewer
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'Viewer'
  ClientHeight = 527
  ClientWidth = 857
  Color = 12510427
  Font.Charset = EASTEUROPE_CHARSET
  Font.Color = clBlack
  Font.Height = -19
  Font.Name = 'Segoe UI'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefault
  ScreenSnap = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 25
  object Shape1: TShape
    Left = 40
    Top = 8
    Width = 65
    Height = 65
    Brush.Style = bsClear
    Pen.Width = 2
  end
  object LineFontL: TLabel
    Left = 464
    Top = 389
    Width = 88
    Height = 25
    Caption = 'LineFontL'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'Segoe UI'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = False
  end
  object DepHourFontL: TLabel
    Left = 464
    Top = 420
    Width = 111
    Height = 21
    Caption = 'DepHourFontL'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clWhite
    Font.Height = -16
    Font.Name = 'Segoe UI'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = False
  end
  object DepFontL: TLabel
    Left = 464
    Top = 474
    Width = 71
    Height = 21
    Caption = 'DepFontL'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clGray
    Font.Height = -16
    Font.Name = 'Segoe UI Semibold'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object DepLineFontL: TLabel
    Left = 464
    Top = 447
    Width = 96
    Height = 21
    Caption = 'DepLineFontL'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clWhite
    Font.Height = -16
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object P1: TPanel
    Left = 8
    Top = 32
    Width = 362
    Height = 441
    BevelOuter = bvNone
    Color = 10212808
    ParentBackground = False
    TabOrder = 0
    object titleL: TLabel
      Left = 16
      Top = 16
      Width = 112
      Height = 30
      Caption = 'RAT Brasov'
      Font.Charset = EASTEUROPE_CHARSET
      Font.Color = clBlack
      Font.Height = -21
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object subtitleL: TLabel
      Left = 16
      Top = 45
      Width = 256
      Height = 15
      Caption = '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
      Font.Charset = EASTEUROPE_CHARSET
      Font.Color = 3426627
      Font.Height = -12
      Font.Name = 'Segoe UI'
      Font.Style = []
      ParentFont = False
    end
    object Shape2: TShape
      Left = 40
      Top = 8
      Width = 65
      Height = 65
      Brush.Style = bsClear
      Pen.Width = 2
    end
    object P3: TPanel
      Left = 32
      Top = 104
      Width = 313
      Height = 289
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 0
        Top = 0
        Width = 253
        Height = 15
        Caption = 'Select the station from which you are departing:'
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = 3426627
        Font.Height = -12
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Left = 0
        Top = 163
        Width = 211
        Height = 15
        Caption = 'Now select the lines you'#39'd like included:'
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = 3426627
        Font.Height = -12
        Font.Name = 'Segoe UI'
        Font.Style = []
        ParentFont = False
      end
      object StationE: TEdit
        Left = 0
        Top = 21
        Width = 265
        Height = 25
        BevelInner = bvNone
        BevelOuter = bvNone
        BorderStyle = bsNone
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Segoe UI Semibold'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        Text = 'Edit1qpW'
        OnChange = StationEChange
      end
      object StationLB: TListBox
        Left = 0
        Top = 52
        Width = 161
        Height = 97
        BorderStyle = bsNone
        Color = 13495268
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Segoe UI Light'
        Font.Style = []
        ItemHeight = 21
        Items.Strings = (
          'hello'
          'kitty'
          'Line 5M')
        ParentFont = False
        TabOrder = 1
        OnClick = StationLBClick
      end
      object LineLB: TListBox
        Left = 0
        Top = 184
        Width = 161
        Height = 84
        BorderStyle = bsNone
        Color = 13495268
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Segoe UI Light'
        Font.Style = []
        ItemHeight = 21
        Items.Strings = (
          'hello'
          'kitty'
          'Line 5M')
        MultiSelect = True
        ParentFont = False
        TabOrder = 2
        OnClick = LineLBClick
      end
    end
  end
  object P2: TScrollBox
    Left = 392
    Top = 157
    Width = 257
    Height = 173
    BorderStyle = bsNone
    Color = 8703164
    ParentColor = False
    TabOrder = 1
    object Shape3: TShape
      Left = 40
      Top = 8
      Width = 80
      Height = 30
      Pen.Width = 2
      Visible = False
    end
    object Label4: TLabel
      Left = 42
      Top = 8
      Width = 18
      Height = 21
      Caption = '30'
      Font.Charset = EASTEUROPE_CHARSET
      Font.Color = clGray
      Font.Height = -16
      Font.Name = 'Segoe UI Semibold'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
  end
  object ModePM: TPopupMenu
    Left = 480
    Top = 16
    object TimetableModeB: TMenuItem
      Caption = 'Timetable mode'
      OnClick = TimetableModeBClick
    end
  end
end
