program RATBrasov;

uses
  Vcl.Forms,
  Main in 'Main.pas' {FMain},
  DataTypes in 'DataTypes.pas',
  About in 'About.pas' {FAbout},
  Updater in 'Updater.pas' {FUpdater},
  Loading in 'Loading.pas' {FLoading},
  Database in 'Database.pas',
  DepartureTime in 'DepartureTime.pas',
  Timetable in 'Timetable.pas',
  Stop in 'Stop.pas',
  Line in 'Line.pas',
  Viewer in 'Viewer.pas' {FViewer},
  ViewerAux in 'ViewerAux.pas',
  CompositeTimetable in 'CompositeTimetable.pas',
  TitleImage in 'TitleImage.pas',
  TimetableImage in 'TimetableImage.pas',
  ViewerSettings in 'ViewerSettings.pas' {FViewerSettings};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFMain, FMain);
  Application.CreateForm(TFAbout, FAbout);
  Application.CreateForm(TFUpdater, FUpdater);
  Application.CreateForm(TFLoading, FLoading);
  Application.CreateForm(TFViewer, FViewer);
  Application.CreateForm(TFViewerSettings, FViewerSettings);
  Application.Run;
end.
