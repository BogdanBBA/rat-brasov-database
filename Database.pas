unit Database;

interface

uses
	Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
	Dialogs, Generics.Collections, UIntList, XML.VerySimple, Line;

type
	TDatabase = class
	private
		theLineList, theStationList, theVehicleList: TStringList;
	public
		Lines: TList<TLine>;
		ViewSettings: TXMLVerySimple;
		property LineList: TStringList read theLineList;
		property StationList: TStringList read theStationList;
		property VehicleList: TStringList read theVehicleList;
		constructor Create;
		destructor Free;
		function TotalDepartures: Integer;
		function TotalTimetables: Integer;
		function TotalStops: Integer;
		function TotalLines: Integer;
		procedure ToFile(FilePath: string);
	end;

implementation

uses DataTypes;

constructor TDatabase.Create;
begin
	theLineList := TStringList.Create;
	theLineList.Sorted := True;
	theLineList.Duplicates := dupIgnore;
	theStationList := TStringList.Create;
	theStationList.Sorted := True;
	theStationList.Duplicates := dupIgnore;
	theVehicleList := TStringList.Create;
	theVehicleList.Sorted := True;
	theVehicleList.Duplicates := dupIgnore;
	Lines := TList<TLine>.Create;
	ViewSettings := TXMLVerySimple.Create
end;

destructor TDatabase.Free;
begin
	FreeAndNil(theLineList);
	FreeAndNil(theStationList);
	FreeAndNil(theVehicleList);
	FreeAndNil(Lines)    ; ViewSettings.Free
end;

function TDatabase.TotalDepartures: Integer;
var N, i: Word;
begin
	N := 0;
	if Lines.Count > 0 then
		for i := 0 to Lines.Count - 1 do
			Inc(N, Lines[i].TotalDepartures);
	Result := N
end;

function TDatabase.TotalTimetables: Integer;
var N, i: Word;
begin
	N := 0;
	if Lines.Count > 0 then
		for i := 0 to Lines.Count - 1 do
			Inc(N, Lines[i].TotalTimetables);
	Result := N
end;

function TDatabase.TotalStops: Integer;
var N, i: Word;
begin
	N := 0;
	if Lines.Count > 0 then
		for i := 0 to Lines.Count - 1 do
			Inc(N, Lines[i].TotalStops);
	Result := N
end;

function TDatabase.TotalLines: Integer;
begin
	Result := Lines.Count
end;

procedure TDatabase.ToFile(FilePath: string);
var i: Word; S: string; F: TextFile;
begin
	S := 'DATABASE' + NL;
	if Lines.Count > 0 then
		for i := 0 to Lines.Count - 1 do
			S := Format('%s @ %s', [S, Lines[i].ToString]);
	AssignFile(F, FilePath);
	ReWrite(F);
	WriteLn(F, S);
	CloseFile(F)
end;

end.
