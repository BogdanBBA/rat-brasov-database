unit DataTypes;

interface

uses
	Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
	Dialogs, UIntList, XML.VerySimple, Database;

const
	NL = #13#10; DNL = NL + NL;

	FormColor = $00BEE4DB;
	PanelColor1 = FormColor;
	PanelColor2 = PanelColor1;
	ControlColor = $00A4D9CD;
	ControlMOColor = $00AEDDD2;
	EditColor = $00CDEBE4;
	TitleBarColor = $00323D32;
	DepartureHourColor = TitleBarColor;

	ButtonWidth = 160;
	ButtonHeight = 50;

	LineLabelWidth = 100;
	LineLabelLineHeight = 40;
	HourColumnWidth = 80;
	DepartureHeight = 30;
	DepartureLineWidth = 35;
	DepartureTimeHalfWidth = 30;
	DepartureTimeFullWidth = 50;

	MinuteListDelimiter = '; ';

	DatabaseFilePath = 'database.xml';
	SettingsFilePath = 'settings.xml';
	TextPresentationFilePath = 'db.txt';

	TimetableModeCaption = 'Timetable mode';
	SelectStationCaption = 'Select the station from which you are departing (%d):';
	SelectLinesCaption = 'Now select the lines you would like included (%d / %d):';
	HaltDownloadText = 'The download process has been halted.';

	TestWebpage = 'http://www.ratbv.ro/trasee1.php';
	LineIndexWebpage: array [1 .. 3] of string = ('http://www.ratbv.ro/trasee2.php?tiptraseu=autobuz',
		'http://www.ratbv.ro/trasee2.php?tiptraseu=microbuz', 'http://www.ratbv.ro/trasee2.php?tiptraseu=troleibuz');
	LineIndexTempFiles: array [1 .. 3] of string = ('bus.html', 'midibus.html', 'trolleybus.html');
	LineDirections: array [0 .. 1] of string = ('dus', 'intors');
	StationListWebpage = 'http://www.ratbv.ro/afisaje/%L-%D/div_list_ro.html';
	TimetableWebpage = 'http://www.ratbv.ro/afisaje/%L-%D/';

	Weekdays: array [1 .. 7] of string = ('luni', 'marti', 'miercuri', 'joi', 'vineri', 'sambata', 'duminica');
	ViewerSubtitles: array [1 .. 4] of string = ('Now in 8-bit colors!', 'Transportation in southern Romania''s finest city',
		'This caption left the station early :(', 'Slowly but surely, from electric tramways to white vans');

var
	DB: TDatabase;

function FormatRouteDescription(S: string): string;
function FormatDays(S: string): string;
function WeekdayPosition(S: string): Word;
function IsInteger(S: string): boolean;
function IsReal(S: string): boolean;
function GetDirectionFromHREF(PageHREF: string): string;
function GetStationListWebpage(Line, Direction: string): string;
function GetTimetableWebpage(Line, Direction, PageHREF: string): string;
procedure MakeEvenNumberOfOpenClosedTags(FilePath, Tag: string);
function ShouldSortLineNames(A, B: string): boolean;

implementation

uses Updater;

function FormatRouteDescription(S: string): string;
var C: Char; P: Integer;
begin
	S := AnsiLowerCase(S);
	for C := 'a' to 'z' do
		begin
			S := StringReplace(S, C + '- ', C + ' - ', [rfReplaceAll]);
			S := StringReplace(S, ' -' + C, ' - ' + C, [rfReplaceAll]);
			S := StringReplace(S, C + '(', C + ' (', [rfReplaceAll]);
			S := StringReplace(S, ')' + C, ') ' + C, [rfReplaceAll]);
			S := StringReplace(S, C + ' )', C + ')', [rfReplaceAll]);
			S := StringReplace(S, '( ' + C, '(' + C, [rfReplaceAll]);
		end;
	S := StringReplace(S, '  ', ' ', [rfReplaceAll]);
	S[1] := UpCase(S[1]);
	for P := 1 to Length(S) - 1 do
		if CharInSet(S[P], [' ', '.', '-', '/', '\', '(']) then
			S[P + 1] := UpCase(S[P + 1]);
	S := StringReplace(S, 'S.A.', 'SA', [rfReplaceAll]);
	S := StringReplace(S, 'A. Vlaicu', 'Aurel Vlaicu', [rfReplaceAll]);
	S := StringReplace(S, 'Ii', 'II', [rfReplaceAll]);
	S := StringReplace(S, 'Cfr', 'CFR', [rfReplaceAll]);
	S := StringReplace(S, 'Ilf', 'ILF', [rfReplaceAll]);
	S := StringReplace(S, 'Iar', 'IAR', [rfReplaceAll]);
	S := StringReplace(S, 'Icpc', 'ICPC', [rfReplaceAll]);
	S := StringReplace(S, 'Rat', 'RAT', [rfReplaceAll]);
	S := StringReplace(S, 'Mttc', 'MTTC', [rfReplaceAll]);
	S := StringReplace(S, 'Ius', 'IUS', [rfReplaceAll]);
	S := StringReplace(S, 'Cet', 'CET', [rfReplaceAll]);
	S := StringReplace(S, 'Cec', 'CEC', [rfReplaceAll]);
	S := StringReplace(S, 'Stadionul', 'Stad.', [rfReplaceAll]);
	S := StringReplace(S, 'Municipal', 'Stad. Municipal', [rfReplaceAll]);
	S := StringReplace(S, '- Tineretului', '- Stad. Tineretului', [rfReplaceAll]);
	S := StringReplace(S, 'Stad.ui', 'Stad. Tineretului', [rfReplaceAll]);
	S := StringReplace(S, 'Timis-Triaj', 'Timis Triaj', [rfReplaceAll]);
	S := StringReplace(S, 'Gara Brasov', 'Gara', [rfReplaceAll]);
	S := StringReplace(S, ' De ', ' de ', [rfReplaceAll]);
	S := StringReplace(S, ' Cel ', ' cel ', [rfReplaceAll]);
	S := StringReplace(S, ' Spre ', ' spre ', [rfReplaceAll]);
	S := StringReplace(S, 'Strada ', '', [rfReplaceAll]);
	S := StringReplace(S, 'Str. ', '', [rfReplaceAll]);
	S := StringReplace(S, 'Bd. ', '', [rfReplaceAll]);
	S := StringReplace(S, 'N. ', 'Nicolae ', [rfReplaceAll]);
	S := StringReplace(S, 'Sc. Gen.', 'Scoala generala', [rfReplaceAll]);
	S := StringReplace(S, 'F-Ca', 'Fabrica', [rfReplaceAll]);
	Result := S
end;

function FormatDays(S: string): string;
begin
	Result := AnsiLowerCase(StringReplace(S, ' ', '', [rfReplaceAll]))
end;

// Returns 0 if not found, 1..7 for monday..sunday
function WeekdayPosition(S: string): Word;
var i: Word;
begin
	S := AnsiLowerCase(S);
	Result := 0;
	for i := 1 to 7 do
		if S = Weekdays[i] then
			begin
				Result := i;
				Exit
			end
end;

function IsInteger(S: string): boolean;
begin
	try
		StrToInt(S);
		Result := True
	except
		Result := False
	end
end;

function IsReal(S: string): boolean;
begin
	try
		StrToFloat(S);
		Result := True
	except
		Result := False
	end
end;

function GetDirectionFromHREF(PageHREF: string): string;
begin
	Result := LineDirections[StrToInt(PageHREF[Pos('cl', PageHREF) + 2]) - 1]
end;

function GetStationListWebpage(Line, Direction: string): string;
var S: string;
begin
	S := StringReplace(StationListWebpage, '%L', Line, [rfReplaceAll]);
	S := StringReplace(S, '%D', Direction, [rfReplaceAll]);
	Result := S
end;

function GetTimetableWebpage(Line, Direction, PageHREF: string): string;
var S: string;
begin
	S := StringReplace(TimetableWebpage, '%L', Line, [rfReplaceAll]);
	S := StringReplace(S, '%D', Direction, [rfReplaceAll]);
	Result := S + PageHREF
end;

procedure MakeEvenNumberOfOpenClosedTags(FilePath, Tag: string);
	function NumberOccurences(S, Text: string): Integer;
	var N: Word;
	begin
		N := 0;
		while Pos(Text, S) <> 0 do
			begin
				Inc(N);
				Delete(S, 1, Pos(Text, S) + Length(Text))
			end;
		Result := N
	end;

var NOpen, NClose, Diff: Integer; SL: TStringList; i: Word;
begin
	SL := TStringList.Create;
	SL.LoadFromFile(FilePath);
	NOpen := NumberOccurences(SL.Text, '<' + Tag);
	NClose := NumberOccurences(SL.Text, '</' + Tag);
	Diff := NOpen - NClose;
	if Diff = 0 then
		Exit;
	// FUpdater.AddLog(3, Format('MakeEvenNumberOfOpenClosedTags(file "%s", tag "%s") HAPPENED (Diff=%d)%s', [FilePath, Tag, Diff, NL]));
	if Diff > 0 then
		for i := 1 to Diff do
			SL.Add('</' + Tag)
	else
		while Diff < 0 do
			begin
				for i := SL.Count - 1 downto 0 do
					if Pos('</' + Tag, SL[i]) <> 0 then
						begin
							Inc(Diff);
							SL.Delete(i);
							Break
						end
			end;
	SL.SaveToFile(FilePath)
end;

function ShouldSortLineNames(A, B: string): boolean;
	procedure LeaveOnlyDigits(var S: string);
	var i: Integer;
	begin
		if Length(S) = 0 then
			Exit;
		i := 1;
		while i <= Length(S) do
			begin
				if not CharInSet(S[i], ['0' .. '9']) then
					begin
						Delete(S, i, 1);
						Inc(i, - 1)
					end;
				Inc(i)
			end
	end;

begin
	LeaveOnlyDigits(A);
	LeaveOnlyDigits(B);
	if (A = '') or (B = '') then
		Result := False
	else
		Result := StrToInt(A) > StrToInt(B)
end;

end.
