unit Loading;

interface

uses
	Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
	Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls,
	ImageButton, DataTypes, XML.VerySimple, StringTokenizer,
	Database, Line, Stop, Timetable, DepartureTime;

type
	TFLoading = class(TForm)
		Shape1: TShape;
		titleL: TLabel;
		subtitleL: TLabel;
		Label1: TLabel;
		Label2: TLabel;
		Label3: TLabel;
		Label0: TLabel;
		Label4: TLabel;
		Label5: TLabel;
		Label6: TLabel;
		Label7: TLabel;
		RE: TRichEdit;
		LoadDBT: TTimer;
		ShowViewerT: TTimer;
		procedure FormCreate(Sender: TObject);
		procedure LoadDBTTimer(Sender: TObject);
		procedure FormShow(Sender: TObject);
		procedure ShowViewerTTimer(Sender: TObject);
	private
		procedure CloseForm(Sender: TObject);
		procedure StopTimers(Sender: TObject);
	public
		procedure AddLog(Priority: Byte; Text: string);
	end;

var
	FLoading: TFLoading;
	CloseB: TImageButton;

	XML: TXMLVerySimple;

implementation

{$R *.dfm}


uses Main, FunctiiBaza, DateUtils, Viewer;

procedure TFLoading.FormCreate(Sender: TObject);
begin
	FLoading.Color := FormColor;
	CloseB := TImageButton.CreateImageButton(FLoading, 'Close', FMain.buttFontL.Font, clBlack, ControlColor, ControlMOColor,
		Rect(FLoading.Width div 2 - ButtonWidth div 2, RE.Top + RE.Height + 20, ButtonWidth, ButtonHeight));
	CloseB.OnClick := CloseForm;
	Shape1.SetBounds(4, 4, FLoading.Width - 8, FLoading.Height - 8)
end;

procedure TFLoading.FormShow(Sender: TObject);
begin
	RE.Clear;
	AddLog(1, 'Attempting to load database from file...' + NL);
	LoadDBT.Enabled := True
end;

procedure TFLoading.CloseForm(Sender: TObject);
begin
	StopTimers(Sender);
	FLoading.Close
end;

procedure TFLoading.StopTimers(Sender: TObject);
begin
	LoadDBT.Enabled := False;
end;

//

procedure TFLoading.AddLog(Priority: Byte; Text: string);
var Comp: TComponent;
begin
	RE.SelStart := Length(RE.Text);
	SendMessage(RE.Handle, EM_SCROLLCARET, 0, 0);
	Comp := FLoading.FindComponent('Label' + IntToStr(Priority));
	if Comp <> nil then
		RE.SelAttributes.Assign(TLabel(Comp).Font)
	else
		RE.SelAttributes.Assign(Label3.Font);
	RE.SelText := Text;
	if (Priority = 0) then
		StopTimers(FLoading)
end;

//

procedure TFLoading.LoadDBTTimer(Sender: TObject);
	procedure TestForSettingsNode(X: TXMLVerySimple; S: string);
	var Node: TXMLNode;
	begin
		if X.Root.Find(S) = nil then
			X.Root.AddChild(S);
		Node := X.Root.Find(S);
		if not Node.HasAttribute('value') then
			Node.SetAttribute('value', '0');
		if not IsInteger(Node.Attribute['value']) then
			Node.Attribute['value'] := '0'
	end;

var Phase: string; ii, iL, iSt, iTT, iClk: Word; d: array [1 .. 7] of boolean; Ok: boolean;
	aName, aDescr, anAux: string; aLength: Real; aH, aM: Word; STok: TStringTokenizer;
	L: TLine; St: TStop; TT: TTimetable; DT: TDepartureTime;
begin
	try
		STok := nil;
		LoadDBT.Enabled := False;
		Phase := 'looking for file';
		if not FileExists(DatabaseFilePath) then
			begin
				AddLog(0, 'The database file "' + DatabaseFilePath + '" does not exist!' + NL);
				Exit
			end;

		Phase := 'loading XML';
		XML := TXMLVerySimple.Create;
		XML.LoadFromFile(DatabaseFilePath);
		AddLog(2, 'The XML was read successfully.' + NL);

		Phase := 'initializing database class';
		DB := TDatabase.Create;

		if XML.Root.ChildNodes.Count = 0 then
			begin
				AddLog(0, 'There is no data in the XML file!' + NL);
				Exit
			end;

		Phase := 'about to parse lines, children=' + IntToStr(XML.Root.ChildNodes.Count);
		if XML.Root.ChildNodes.Count > 0 then
			for iL := 0 to XML.Root.ChildNodes.Count - 1 do
				with XML.Root.ChildNodes[iL] do
					begin
						Phase := Format('parsing lines, iL=%d', [iL]);
						aName := Attribute['name'];
						aDescr := Attribute['routeDescription'];
						anAux := Attribute['type'];
						if IsReal(Attribute['length']) then
							aLength := StrToFloat(Attribute['length'])
						else
							aLength := 0;

						L := TLine.Create(aName, anAux, aDescr, aLength);
						Phase := Format('created line %s; about to parse stations, children=%d', [L.Name, ChildNodes.Count]);

						if ChildNodes.Count > 0 then
							for iSt := 0 to ChildNodes.Count - 1 do
								with ChildNodes[iSt] do
									begin
										Phase := Format('parsing stations, L=%s, iSt=%d', [L.Name, iSt]);
										aName := Attribute['name'];
										anAux := Attribute['href'];

										St := TStop.Create(aName, anAux);
										Phase := Format('created station %s; about to parse timetables, children=%d', [St.Name, ChildNodes.Count]);

										if ChildNodes.Count > 0 then
											for iTT := 0 to ChildNodes.Count - 1 do
												with ChildNodes[iTT] do
													begin
														Phase := Format('parsing timetables, L=%s, St=%s, iTT=%d', [L.Name, St.Name, iTT]);
														for ii := 1 to 7 do
															d[ii] := False;
														anAux := Attribute['days'];
														if Pos('-', anAux) = 0 then
															d[WeekdayPosition(anAux)] := True
														else
															for ii := WeekdayPosition(Copy(anAux, 1, Pos('-', anAux) - 1))
																to WeekdayPosition(Copy(anAux, Pos('-', anAux) + 1, Length(anAux))) do
																d[ii] := True;

														TT := TTimetable.Create(d);
														TT.Description := Format('Line %s, Station %s, Timetable iTT=%d', [L.Name, St.Name, iTT]);
														Phase := Format('created timetable iTT=%d; about to parse clock nodes, children=%d', [iTT, ChildNodes.Count]);

														if ChildNodes.Count > 0 then
															for iClk := 0 to ChildNodes.Count - 1 do
																with ChildNodes[iClk] do
																	begin
																		Phase := Format('parsing clock nodes, L=%s, St=%s, iTT=%d, iClk=%d', [L.Name, St.Name, iTT, iClk]);
																		aH := StrToInt(Attribute['hour']);
																		STok := TStringTokenizer.Create(Attribute['minutes']);
																		while STok.HasMoreTokens do
																			begin
																				DT := TDepartureTime.Create(aH, StrToInt(STok.NextToken(MinuteListDelimiter)));
																				TT.Departures.Add(DT);
																			end;
																	end;

														Phase := Format('adding timetable to station, L=%s, St=%s, iTT=%d', [L.Name, St.Name, iTT]);
														St.Timetables.Add(TT);
													end;

										Phase := Format('adding station to line, L=%s, St=%s', [L.Name, St.Name]);
										L.Stops.Add(St);
									end;

						Phase := Format('adding line to database, L=%s', [L.Name]);
						DB.Lines.Add(L);
					end;

		Phase := 'sorting lines';
		if DB.Lines.Count > 1 then
			for aH := 0 to DB.Lines.Count - 2 do
				for aM := aH + 1 to DB.Lines.Count - 1 do
					if ShouldSortLineNames(DB.Lines[aH].Name, DB.Lines[aM].Name) then
						DB.Lines.Exchange(aH, aM);

		Phase := 'indexing stations in DB.StationList';
		for iL := 0 to DB.Lines.Count - 1 do
			if DB.Lines[iL].Stops.Count > 0 then
				for iSt := 0 to DB.Lines[iL].Stops.Count - 1 do
					DB.StationList.Add(DB.Lines[iL].Stops[iSt].Name);

		Phase := 'indexing lines in DB.LineList';
		for iL := 0 to DB.Lines.Count - 1 do
			DB.LineList.Add(DB.Lines[iL].Name);

		Phase := 'indexing vehicle types in DB.LineList';
		for iL := 0 to DB.Lines.Count - 1 do
			DB.VehicleList.Add(DB.Lines[iL].VehicleType);

		Phase := 'loading settings';;
				Ok:=True;
		if not FileExists(SettingsFilePath) then
			Ok := False
		else
			try
				DB.ViewSettings.LoadFromFile(SettingsFilePath)
			except
				Ok := False
			end;
		if not Ok then
			begin
				XML := TXMLVerySimple.Create;
				XML.Root.NodeName := 'RATDBSettings'
			end;
		TestForSettingsNode(DB.ViewSettings, 'showTitleImage');
		TestForSettingsNode(DB.ViewSettings, 'showHourColumn');
		TestForSettingsNode(DB.ViewSettings, 'showDepartureLine');
		TestForSettingsNode(DB.ViewSettings, 'showDepartureHour');
//		DB.XML.Root.AddChild('useCurrentDate');
//		DB.XML.Root.ChildNodes.Last.SetAttribute('value', '1');

		Phase := 'presenting information in file';
		AddLog(2, 'Presenting database in file...' + NL);
		DB.ToFile(TextPresentationFilePath);

		Phase := 'freeing temporary memory';
		XML.Free;
		if STok <> nil then
			STok.Free;

		//

		AddLog(2, Format('The database contains %d line(s), %d stop(s), %d timetable(s) and %d departure(s).',
			[DB.TotalLines, DB.TotalStops, DB.TotalTimetables, DB.TotalDepartures]));
		ShowViewerT.Enabled := True
	except
		on E: Exception do
			AddLog(0, Format('LoadDBTTimer ERROR (phase "%s")%s%s: %s%s', [Phase, NL, E.ClassName, E.Message, NL]))
	end
end;

procedure TFLoading.ShowViewerTTimer(Sender: TObject);
begin
	ShowViewerT.Enabled := False;
	FLoading.CloseForm(Sender);
	FViewer.ShowModal;
end;

end.
