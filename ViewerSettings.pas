unit ViewerSettings;

interface

uses
	Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
	Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ImageButton, DataTypes, Vcl.ExtCtrls,
	Vcl.StdCtrls, Vcl.ComCtrls;

type
	TFViewerSettings = class(TForm)
		Shape1: TShape;
		titleL: TLabel;
		subtitleL: TLabel;
		Shape2: TShape;
		ch1: TCheckBox;
		ch2: TCheckBox;
		ch3: TCheckBox;
		ch4: TCheckBox;
		ch5: TCheckBox;
		dtp1: TDateTimePicker;
		dtp2: TDateTimePicker;
		procedure FormCreate(Sender: TObject);
		procedure ch5Click(Sender: TObject);
		procedure FormShow(Sender: TObject);
		procedure FormClose(Sender: TObject; var Action: TCloseAction);
	private
		procedure CloseForm(Sender: TObject);
		procedure SaveData(Sender: TObject);
	public
		{ Public declarations }
	end;

var
	FViewerSettings: TFViewerSettings;
	SaveB, CloseB: TImageButton;

implementation

{$R *.dfm}


uses Main, DateUtils, Viewer;

procedure TFViewerSettings.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	FViewer.LineLBClick(Sender)
end;

procedure TFViewerSettings.FormCreate(Sender: TObject);
begin
	FViewerSettings.Color := FormColor;
	CloseB := TImageButton.CreateImageButton(FViewerSettings, 'Close', FMain.buttFontL.Font, clBlack, ControlColor, ControlMOColor,
		Rect(FViewerSettings.Width div 2 + 5, Shape2.Top, Shape2.Width, Shape2.Height));
	CloseB.OnClick := CloseForm;
	SaveB := TImageButton.CreateImageButton(FViewerSettings, 'Save', FMain.buttFontL2.Font, clBlack, ControlColor, ControlMOColor,
		Rect(FViewerSettings.Width div 2 - Shape2.Width - 5, Shape2.Top, Shape2.Width, Shape2.Height));
	SaveB.OnClick := SaveData;
	FViewerSettings.Height := CloseB.Top + CloseB.Height + 40;
	Shape1.SetBounds(4, 4, FViewerSettings.Width - 8, FViewerSettings.Height - 8);
	dtp1.DateTime := Now;
	while DayofTheWeek(dtp1.DateTime) <> 7 do
		dtp1.DateTime := IncDay(dtp1.DateTime);
	dtp2.DateTime := 0;
	ch5.Checked := True
end;

procedure TFViewerSettings.FormShow(Sender: TObject);
begin
	ch1.Checked := DB.ViewSettings.Root.Find('showTitleImage').Attribute['value'] = '1';
	ch2.Checked := DB.ViewSettings.Root.Find('showHourColumn').Attribute['value'] = '1';
	ch3.Checked := DB.ViewSettings.Root.Find('showDepartureLine').Attribute['value'] = '1';
	ch4.Checked := DB.ViewSettings.Root.Find('showDepartureHour').Attribute['value'] = '1'
end;

procedure TFViewerSettings.ch5Click(Sender: TObject);
begin
	dtp1.Enabled := not ch5.Checked;
	dtp2.Enabled := dtp1.Enabled
end;

procedure TFViewerSettings.CloseForm(Sender: TObject);
begin
	FViewerSettings.Close
end;

procedure TFViewerSettings.SaveData(Sender: TObject);
begin
	DB.ViewSettings.Root.Find('showTitleImage').Attribute['value'] := IntToStr(Integer(ch1.Checked));
	DB.ViewSettings.Root.Find('showHourColumn').Attribute['value'] := IntToStr(Integer(ch2.Checked));
	DB.ViewSettings.Root.Find('showDepartureLine').Attribute['value'] := IntToStr(Integer(ch3.Checked));
	DB.ViewSettings.Root.Find('showDepartureHour').Attribute['value'] := IntToStr(Integer(ch4.Checked));
	DB.ViewSettings.SaveToFile(SettingsFilePath);
	CloseForm(Sender)
end;

end.
