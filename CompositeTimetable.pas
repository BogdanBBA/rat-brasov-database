unit CompositeTimetable;

interface

uses
	Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
	Vcl.ExtCtrls, Vcl.StdCtrls,
	Dialogs, Generics.Collections, ViewerAux, Line, DepartureTime, TitleImage, TimetableImage;

type
	TCompositeTimetable = class
	private

	public
		Station: string;
		VLines: TList<TViewerLine>;
		IDDeps: TList<TIdentityDepartureTime>;

		TitleImg: TTitleImage;
		TTI: TTimetableImage;

		constructor Create(ImageParent: TWinControl);
		destructor Free;

		procedure RefreshFilledInCompositeTimetable;
	end;

implementation

uses Viewer, DataTypes;

constructor TCompositeTimetable.Create(ImageParent: TWinControl);
begin
	VLines := TList<TViewerLine>.Create;
	IDDeps := TList<TIdentityDepartureTime>.Create;
	TitleImg := TTitleImage.Create(ImageParent);
	TTI := TTimetableImage.Create(ImageParent)
end;

destructor TCompositeTimetable.Free;
var Phase: string;
begin
	try
		Phase := 'freeing VLines';
		FreeAndNil(VLines);
		Phase := 'freeing IDDepartures';
		FreeAndNil(IDDeps);
		Phase := 'freeing TitleImg';
		FreeAndNil(TitleImg);
		Phase := 'freeing TTI';
		FreeAndNil(TTI)
	except
		on E: Exception do
			MessageDlg(Format('An error has occured in method TCompositeTimetable.Free (phase: "%s")', [Phase]), mtError, [mbIgnore], 0)
	end
end;

procedure TCompositeTimetable.RefreshFilledInCompositeTimetable;
var nLines, LperL, i, j, L, T, R, B: Integer;
	nHours: Integer; depPerH: array [0 .. 23] of Word; maxDep, HColAdd, depW, hourH,  lastHour: Word;
	TIR: TTitleImageRegion; TTIR: TTimetableImageRegion; TTIHR: TTimetableImageHourRegion;
begin
	// TITLE IMAGE

	// Sort departure times
	if IDDeps.Count > 1 then
		for i := 0 to IDDeps.Count - 2 do
			for j := i + 1 to IDDeps.Count - 1 do
				if CompareDepartureTimes(IDDeps[i], IDDeps[j]) = - 1 then
					IDDeps.Exchange(i, j);
	// Calculate title image size
	LperL := TitleImg.Width div LineLabelWidth;
	nLines := 1;
	while nLines * LperL < VLines.Count do
		Inc(nLines);
	TitleImg.Height := nLines * LineLabelLineHeight;
	// Create title image regions
	TitleImg.ClearRegions;
	L := 0;
	T := 0;
	R := L + LineLabelWidth;
	B := T + LineLabelLineHeight;
	for i := 0 to VLines.Count - 1 do
		begin
			if R > TitleImg.Width then
				begin
					Inc(T, LineLabelLineHeight);
					B := T + LineLabelLineHeight;
					L := 0;
					R := L + LineLabelWidth
				end;
			TIR := TTitleImageRegion.Create;
			TIR.R := Rect(L, T, R, B);
			TIR.Ln := VLines[i];
			TitleImg.Regions.Add(TIR);
			Inc(L, LineLabelWidth);
			Inc(R, LineLabelWidth)
		end;
	// Show and refresh title image
	TitleImg.Show;
	TitleImg.RefreshTitleImage;

  // TIMETABLE IMAGE

	// Create timetable image regions
	TTI.ClearRegions;
	if IDDeps.Count > 0 then
		for i := 0 to IDDeps.Count - 1 do
			begin
				TTIR := TTimetableImageRegion.Create;
				TTIR.Dep := IDDeps[i];
				TTI.Regions.Add(TTIR)
			end;
	if IDDeps.Count > 1 then
		begin
			// Get number of hours and the maximum number of departures per hour
			nHours := IDDeps[IDDeps.Count - 1].Hour - IDDeps[0].Hour + 1;
			for i := 0 to 23 do
				depPerH[i] := 0;
			for i := 0 to IDDeps.Count - 1 do
				Inc(depPerH[IDDeps[i].Hour]);
			maxDep := depPerH[0];
			for i := 1 to 23 do
				if maxDep < depPerH[i] then
					maxDep := depPerH[i];
			// Now construct the width of a region
			HColAdd := HourColumnWidth * Integer(DB.ViewSettings.Root.Find('showHourColumn').Attribute['value'] = '1');
			if DB.ViewSettings.Root.Find('showDepartureLine').Attribute['value'] = '1' then
				depW := DepartureLineWidth
			else
				depW := 0;
			if DB.ViewSettings.Root.Find('showDepartureHour').Attribute['value'] = '1' then
				Inc(depW, DepartureTimeFullWidth)
			else
				Inc(depW, DepartureTimeHalfWidth);
			// Now get the hourly and total heights
			hourH := DepartureHeight;
			L := 0;
			R := L + depW;
			for i := 1 to maxDep do
				begin
					if R > TTI.Width then
						begin
							Inc(hourH, DepartureHeight);
							L := 0;
							R := L + depW
						end;
					Inc(L, depW);
					Inc(R, depW)
				end;
			TTI.SetBounds(0, TitleImg.Top + TitleImg.Height + 10, TitleImg.Width, nHours * hourH + 1);
			// Now set hourly size
			TTI.HRegions.Clear;
			if DB.ViewSettings.Root.Find('showHourColumn').Attribute['value'] = '1' then
				for i := 0 to nHours - 1 do
					begin
						TTIHR := TTimetableImageHourRegion.Create;
						TTIHR.R := Rect(0, i * hourH, HourColumnWidth, (i + 1) * hourH);
						TTIHR.HourText := Format('%.2d H', [IDDeps[0].Hour + i]);
						TTI.HRegions.Add(TTIHR)
					end;
			// Now set each region
			lastHour := IDDeps[0].Hour;
			L := HColAdd;
			R := L + depW;
			T := 0;
			B := T + DepartureHeight;
			for i := 0 to IDDeps.Count - 1 do
				begin
					if (IDDeps[i].Hour <> lastHour) or (R > TTI.Width) then
						begin
							L := HColAdd;
							R := L + depW;
							Inc(T, DepartureHeight);
							Inc(B, DepartureHeight);
							lastHour := IDDeps[i].Hour
						end;
					TTI.Regions[i].R := Rect(L, T, R + 1, B + 1);
					Inc(L, depW);
					Inc(R, depW)
				end;

		end;
	//
	TTI.Show;
	TTI.RefreshTimetableImage
end;

end.
