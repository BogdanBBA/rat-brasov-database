unit TitleImage;

interface

uses
	Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
	Vcl.ExtCtrls, Vcl.StdCtrls,
	Dialogs, Generics.Collections, ViewerAux, Line, DepartureTime;

type
	TTitleImageRegion = class
	public
		R: TRect;
		Ln: TViewerLine;
		constructor Create;
	end;

type
	TTitleImage = class(TImage)
	private
		LastMousePos: TPoint;
	public
		Regions: TList<TTitleImageRegion>;
		constructor Create(aParent: TWinControl);
		destructor Free;
		property GetLastMousePos: TPoint read LastMousePos;
		procedure ClearRegions;
		procedure TestPaint;
		procedure RefreshTitleImageRegion(Reg: TTitleImageRegion; MouseOver: boolean);
		procedure TitleImageOnMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
		procedure TitleImageOnMouseEnter(Sender: TObject);
		procedure TitleImageOnMouseLeave(Sender: TObject);
		procedure TitleImageOnClick(Sender: TObject);
		procedure RefreshTitleImage;
	end;

implementation

uses DataTypes, Viewer;

constructor TTitleImageRegion.Create;
begin
	Ln := TViewerLine.Create;
end;

//

constructor TTitleImage.Create(aParent: TWinControl);
begin
	inherited Create(aParent);
	OnMouseEnter := TitleImageOnMouseEnter;
	OnMouseLeave := TitleImageOnMouseLeave;
	OnMouseMove := TitleImageOnMouseMove;
	OnClick := TitleImageOnClick;
	Parent := aParent;
	Regions := TList<TTitleImageRegion>.Create
end;

destructor TTitleImage.Free;
var Phase: string;
begin
	try
		Phase := 'freeing Regions';
		FreeAndNil(Regions);
		Phase := 'freeing inherited TImage';
		// inherited Free
	except
		on E: Exception do
			MessageDlg(Format('An error has occured in method TTitleImage.Free (phase: "%s")', [Phase]), mtError, [mbIgnore], 0)
	end
end;

//

procedure TTitleImage.ClearRegions;
var i: Word;
begin
	if Regions.Count > 0 then
		for i := 0 to Regions.Count - 1 do
			Regions[i].Free;
	Regions.Clear
end;

//

procedure TTitleImage.TestPaint;
var i, j: Word;
begin
	Picture := nil;
	for i := 0 to Width - 1 do
		for j := 0 to Height - 1 do
			Canvas.Pixels[i, j] := RGB(Random(j), Random(i), 32 * Random(8) - 1)
end;

procedure TTitleImage.RefreshTitleImageRegion(Reg: TTitleImageRegion; MouseOver: boolean);
var S: string;
begin
	with Canvas do
		begin
			Brush.Color := TitleBarColor;
			Brush.Style := bsSolid;
			FillRect(Reg.R);
			Font.Assign(FViewer.LineFontL.Font);
			S := 'Line ' + DB.Lines[Reg.Ln.Line].Name;
			if MouseOver then
				begin
					Font.Color := ControlColor;
					// Font.Style := Font.Style + [fsUnderline]
				end;
			TextOut(Reg.R.Left + Reg.R.Width div 2 - TextWidth(S) div 2, Reg.R.Top + Reg.R.Height div 2 - TextHeight(S) div 2, S)
		end
end;

procedure TTitleImage.TitleImageOnMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
var i: Word;
begin
	LastMousePos := Point(X, Y);
	if Regions.Count > 0 then
		for i := 0 to Regions.Count - 1 do
			begin
				//Showmessagefmt('i=%d, T=%d, B=%d', [i, Regions[i].R.Top, Regions[i].R.Bottom]);
				if Regions[i].R.Contains(LastMousePos) then
					RefreshTitleImageRegion(Regions[i], True)
				else
					RefreshTitleImageRegion(Regions[i], False)
			end
end;

procedure TTitleImage.TitleImageOnMouseEnter(Sender: TObject);
begin
	// TitleImageOnMouseMove(Sender, [], - 1, - 1)
end;

procedure TTitleImage.TitleImageOnMouseLeave(Sender: TObject);
begin
	TitleImageOnMouseMove(Sender, [], - 1, - 1)
end;

procedure TTitleImage.TitleImageOnClick(Sender: TObject);
var i: Word;
begin
	if Regions.Count > 0 then
		for i := 0 to Regions.Count - 1 do
			if Regions[i].R.Contains(LastMousePos) then
				with DB.Lines[Regions[i].Ln.Line] do
					begin
						MessageDlg(Format('Line %s%sFrom "%s" to "%s"', [Name, DNL, Stops[Regions[i].Ln.DepartureStation].Name, Stops[Regions[i].Ln.ArrivalStation].Name]),
							mtInformation, [mbOK], 0);
						Break
					end
end;

procedure TTitleImage.RefreshTitleImage;
var i: Integer;
begin
	Picture := nil;
	with Canvas do
		begin
			Brush.Color := TitleBarColor;
			Brush.Style := bsSolid;
			FillRect(Rect(0, 0, Width, Height))
		end;
	if Regions.Count > 0 then
		for i := 0 to Regions.Count - 1 do
			RefreshTitleImageRegion(Regions[i], False)
end;

end.
