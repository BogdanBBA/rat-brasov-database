unit Viewer;

interface

uses
	Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
	Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls, Vcl.Menus,
	Generics.Collections, ImageButton, DataTypes, XML.VerySimple, StringTokenizer,
	Database, Line, Stop, Timetable, DepartureTime, ViewerAux, CompositeTimetable;

type
	TFViewer = class(TForm)
		P1: TPanel;
		titleL: TLabel;
		subtitleL: TLabel;
		Shape1: TShape;
		Shape2: TShape;
		ModePM: TPopupMenu;
		TimetableModeB: TMenuItem;
		P3: TPanel;
		Label1: TLabel;
		StationE: TEdit;
		Label2: TLabel;
		StationLB: TListBox;
		LineLB: TListBox;
		P2: TScrollBox;
		LineFontL: TLabel;
		Shape3: TShape;
		DepHourFontL: TLabel;
		Label4: TLabel;
		DepFontL: TLabel;
    DepLineFontL: TLabel;
		procedure FormCreate(Sender: TObject);
		procedure FormResize(Sender: TObject);
		procedure TimetableModeBClick(Sender: TObject);
		procedure FormShow(Sender: TObject);
		procedure StationEChange(Sender: TObject);
		procedure StationLBClick(Sender: TObject);
		procedure LineLBClick(Sender: TObject);
		procedure FormClose(Sender: TObject; var Action: TCloseAction);
	private
		procedure CloseForm(Sender: TObject);
		procedure OpenOptionsMenu(Sender: TObject);
		procedure OpenModeMenu(Sender: TObject);
	public
		{ Public declarations }
	end;

var
	FViewer: TFViewer;

	CloseB, OptionsB, ModeB: TImageButton;
	LineList: TList<TViewerLine>;
	CTT: TCompositeTimetable;

implementation

{$R *.dfm}


uses Main, FunctiiBaza, DateUtils, ViewerSettings;

//

procedure TFViewer.CloseForm(Sender: TObject);
begin
	FViewer.Close
end;

procedure TFViewer.OpenOptionsMenu(Sender: TObject);
var P: TPoint;
begin
	FViewerSettings.ShowModal
end;

procedure TFViewer.OpenModeMenu(Sender: TObject);
var P: TPoint;
begin
	GetCursorPos(P);
	ModePM.Popup(P.X, P.Y)
end;

procedure TFViewer.TimetableModeBClick(Sender: TObject);
begin
	ModeB.Caption := TimetableModeCaption;
	ModeB.RefreshIB;
	P3.Show; // any other P's will be hidden
end;

//

procedure TFViewer.FormClose(Sender: TObject; var Action: TCloseAction);
var Phase: string;
begin
	try
		Phase := 'freeing LineList';
		LineList.Free;
		Phase := 'freeing CTT';
		CTT.Free;
		Phase := 'freeing DB';
		DB.Free
	except
		on E: Exception do
			MessageDlg(Format('An error has occured in method TFViewer.FormClose (phase: "%s")', [Phase]), mtError, [mbIgnore], 0)
	end
end;

procedure TFViewer.FormCreate(Sender: TObject);
begin
	try
		CTT := TCompositeTimetable.Create(FViewer.P2);
		LineList := TList<TViewerLine>.Create;

		FViewer.Color := FormColor;
		P1.Color := PanelColor1;
		P2.Color := PanelColor2;
		P3.Color := P1.Color;
		StationE.Color := EditColor;

		CloseB := TImageButton.CreateImageButton(P1, 'Close', FMain.buttFontL2.Font, clBlack, ControlColor, ControlMOColor,
			Rect(10, 10, ButtonWidth, ButtonHeight));
		CloseB.OnClick := CloseForm;
		OptionsB := TImageButton.CreateImageButton(P1, 'Options', FMain.buttFontL.Font, clBlack, ControlColor, ControlMOColor,
			Rect(10, 10, ButtonWidth, ButtonHeight));
		OptionsB.OnClick := OpenOptionsMenu;
		ModeB := TImageButton.CreateImageButton(P1, 'Mode (uninitialized)', FMain.buttFontL.Font, clBlack, ControlColor, ControlMOColor,
			Rect(10, 10, ButtonWidth, ButtonHeight));
		ModeB.OnClick := OpenModeMenu;
	except
		on E: Exception do
			MessageDlg(Format('An error has occured in method TFViewer.FormCreate:%s%s: %s', [DNL, E.ClassName, E.Message]), mtError, [mbOK], 0)
	end
end;

procedure TFViewer.FormShow(Sender: TObject);
begin
	try
		subtitleL.Caption := ViewerSubtitles[random(Length(ViewerSubtitles)) + 1];
		TimetableModeBClick(Sender);
		StationE.Text := '';
		StationE.OnChange(StationE)
	except
		on E: Exception do
			MessageDlg(Format('An error has occured in method TFViewer.FormShow:%s%s: %s', [DNL, E.ClassName, E.Message]), mtError, [mbOK], 0)
	end
end;

procedure TFViewer.FormResize(Sender: TObject);
begin
	try
		Shape1.SetBounds(4, 4, FViewer.Width - 8, FViewer.Height - 8);

		P1.SetBounds(3 * Shape1.Left, 3 * Shape1.Top, 362, Shape1.Height - 4 * Shape1.Top);
		Shape2.SetBounds(4, 4, P1.Width - 8, P1.Height - 8);
		CloseB.SetBounds(P1.Width div 2 - ButtonWidth - 5, P1.Height - CloseB.Height - 20, ButtonWidth, ButtonHeight);
		OptionsB.SetBounds(P1.Width div 2 + 3, CloseB.Top, ButtonWidth, ButtonHeight);

		ModeB.SetBounds(subtitleL.Left, subtitleL.Top + subtitleL.Height + 10, P1.Width - 2 * subtitleL.Left, ButtonHeight);
		ModeB.RefreshIB;
		P3.SetBounds(ModeB.Left, ModeB.Top + ModeB.Height + 10, ModeB.Width, CloseB.Top - ModeB.Height - ModeB.Top - 20);
		StationE.Width := StationE.Parent.Width;
		StationLB.Width := StationE.Width;
		LineLB.Width := StationE.Width;
		LineLB.Height := P3.Height - LineLB.Top;

		P2.SetBounds(P1.Left + P1.Width + 3 * Shape1.Left, P1.Top, FViewer.Width - P1.Width - 8 * Shape1.Left, P1.Height);
		CTT.TitleImg.SetBounds(0, 0, P2.Width - GetSystemMetrics(SM_CYHSCROLL), 50);
		CTT.TitleImg.TestPaint;
		CTT.TTI.SetBounds(0, CTT.TitleImg.Top + CTT.TitleImg.Height + 10, CTT.TitleImg.Width, 50);
		CTT.TTI.TestPaint
	except
		on E: Exception do
			MessageDlg(Format('An error has occured in method TFViewer.FormResize:%s%s: %s', [DNL, E.ClassName, E.Message]), mtError, [mbOK], 0)
	end
end;

//

procedure TFViewer.StationEChange(Sender: TObject);
var S, Phase: string; i: Word;
begin
	try
		Phase := 'getting TTedit.Text';
		S := AnsiLowerCase(StationE.Text);
		StationLB.Items.Clear;
		Phase := 'about to iterate; s is "' + S + '"';
		if S = '' then
			for i := 0 to DB.StationList.Count - 1 do
				StationLB.Items.Add(DB.StationList[i])
		else
			for i := 0 to DB.StationList.Count - 1 do
				if Pos(S, AnsiLowerCase(DB.StationList[i])) <> 0 then
					StationLB.Items.Add(DB.StationList[i]);
		Phase := 'iterated; added ' + IntToStr(StationLB.Items.Count) + ' item(s); setting itemindex';
		Label1.Caption := Format(SelectStationCaption, [StationLB.Items.Count]);
		if StationLB.Items.Count > 0 then
			StationLB.ItemIndex := 0;
		Phase := 'calling StationLB.OnClick(Sender)';
		StationLB.OnClick(Sender)
	except
		on E: Exception do
			MessageDlg(Format('An error has occured in method TFViewer.StationEChange (phase "%s"):%s%s: %s',
				[Phase, DNL, E.ClassName, E.Message]), mtError, [mbOK], 0)
	end
end;

procedure TFViewer.StationLBClick(Sender: TObject);
var i, iL, iSt: Word; Phase: string; A, B: Integer;
begin
	try
		Phase := 'initializing';
		LineLB.Items.Clear;
		LineList.Clear;
		if StationLB.ItemIndex = - 1 then
			Exit;
		Phase := 'iterating lines, then stations';
		for iL := 0 to DB.Lines.Count - 1 do
			if DB.Lines[iL].Stops.Count > 0 then
				for iSt := 0 to DB.Lines[iL].Stops.Count - 1 do
					if DB.Lines[iL].Stops[iSt].Name = StationLB.Items[StationLB.ItemIndex] then
						if DB.Lines[iL].GetRouteForStationIndex(iSt, A, B) then
							if StationLB.Items[StationLB.ItemIndex] <> DB.Lines[iL].Stops[B].Name then
								LineList.Add(TViewerLine.Create(iL, A, B));

		Phase := 'adding items to LineLB';
		if LineList.Count > 0 then
			for i := 0 to LineList.Count - 1 do
				LineLB.Items.Add(Format('Line %s (%s -> %s)', [DB.Lines[LineList[i].Line].Name,
					DB.Lines[LineList[i].Line].Stops[LineList[i].DepartureStation].Name, DB.Lines[LineList[i].Line].Stops[LineList[i].ArrivalStation].Name]));
		LineLBClick(Sender)
	except
		on E: Exception do
			MessageDlg(Format('An error has occured in method TFViewer.StationLBClick (phase "%s"):%s%s: %s',
				[Phase, DNL, E.ClassName, E.Message]), mtError, [mbOK], 0)
	end
end;

procedure TFViewer.LineLBClick(Sender: TObject);
var Phase: string; LL: TList<TViewerLine>; i, j, k, P: Integer; D: TDateTime; AuxIDDep: TIdentityDepartureTime;
begin
	try
		Phase := 'initializing';
		i := - 1;
		j := - 1;
		k := - 1;
		P := - 1;
		Label2.Caption := Format(SelectLinesCaption, [LineLB.SelCount, LineLB.Count]);
		if LineLB.Count = 0 then
			Exit;
		LL := TList<TViewerLine>.Create;
		Phase := 'adding selected to LL';
		for i := 0 to LineLB.Count - 1 do
			if LineLB.Selected[i] then
				LL.Add(LineList[i]);
		Phase := 'cleaning up CTT';
		CTT.Station := StationLB.Items[StationLB.ItemIndex];
		CTT.VLines.Clear;
		CTT.VLines.AddRange(LL);
		CTT.IDDeps.Clear;
		Phase := 'filling in the CTT';
		if FViewerSettings.ch5.Checked then
			D := Now
		else
			D := Trunc(FViewerSettings.dtp1.DateTime) + Frac(FViewerSettings.dtp2.DateTime);
		for i := 0 to CTT.VLines.Count - 1 do
			with DB.Lines[CTT.VLines[i].Line] do
				begin
					Phase := 'adding departures';
					P := GetStopIndex(CTT.Station);
					if P <> - 1 then
						if Stops[P].Timetables.Count > 0 then
							for j := 0 to Stops[P].Timetables.Count - 1 do
								if Stops[P].Timetables[j].Days[DayOfTheWeek(D)] then
									if Stops[P].Timetables[j].Departures.Count > 0 then
										for k := 0 to Stops[P].Timetables[j].Departures.Count - 1 do
											CTT.IDDeps.Add(TIdentityDepartureTime.Create(Stops[P].Timetables[j].Departures[k], DB.Lines[CTT.VLines[i].Line].Name, CTT.Station))
				end;
		Phase := 'finished filling in the CTT';
		LL.Free;
		CTT.RefreshFilledInCompositeTimetable
	except
		on E: Exception do
			MessageDlg(Format('An error has occured in method TFViewer.LineLBClick (phase "%s", i=%d, P=%d, j=%d, k=%d):%s%s: %s',
				[Phase, i, P, j, k, DNL, E.ClassName, E.Message]), mtError, [mbOK], 0)
	end
end;

end.
