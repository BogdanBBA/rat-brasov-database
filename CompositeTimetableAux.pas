unit CompositeTimetableAux;

interface

uses
	Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
	Vcl.ExtCtrls, Vcl.StdCtrls,
	Dialogs, Generics.Collections, ViewerAux, Line, DepartureTime;

type
	TTitleImageRegion = class
	public
		R: TRect;
		Ln: TViewerLine;
		constructor Create;
	end;

type
	TTimetableImageRegion = class
	public
		R: TRect;
		Dep: TIdentityDepartureTime;
		constructor Create;
	end;

type
	TTitleImage = class(TImage)
		Regions: TList<TTitleImageRegion>;
		constructor Create(aParent: TWinControl);
		destructor Free;
		procedure ClearRegions;
		procedure RefreshTitleImageRegion(Reg: TTitleImageRegion; MouseOver: boolean);
		procedure TitleImageOnMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
		procedure TitleImageOnMouseEnter(Sender: TObject);
		procedure TitleImageOnMouseLeave(Sender: TObject);
		procedure RefreshTitleImage;
	end;

type
	TTimetableImage = class(TImage)
		Regions: TList<TTimetableImageRegion>;
		constructor Create(aParent: TWinControl);
		destructor Free;
		procedure ClearRegions;
		procedure RefreshTimetableImage;
	end;

implementation

uses DataTypes, Viewer;

constructor TTitleImageRegion.Create;
begin
	Ln := TViewerLine.Create;
end;

constructor TTimetableImageRegion.Create;
begin
	Dep := TIdentityDepartureTime.Create
end;

//

constructor TTitleImage.Create(aParent: TWinControl);
begin
	inherited Create(aParent);
	OnMouseEnter := TitleImageOnMouseEnter;
	OnMouseLeave := TitleImageOnMouseLeave;
	OnMouseMove := TitleImageOnMouseMove;
	Parent := aParent;
	Regions := TList<TTitleImageRegion>.Create
end;

destructor TTitleImage.Free;
var Phase: string;
begin
	try
		Phase := 'freeing Regions';
		FreeAndNil(Regions);
		Phase := 'freeing inherited TImage';
		// inherited Free
	except
		on E: Exception do
			MessageDlg(Format('An error has occured in method TTitleImage.Free (phase: "%s")', [Phase]), mtError, [mbIgnore], 0)
	end
end;

constructor TTimetableImage.Create(aParent: TWinControl);
begin
	inherited Create(aParent);
	Parent := aParent;
	Regions := TList<TTimetableImageRegion>.Create
end;

destructor TTimetableImage.Free;
var Phase: string;
begin
	try
		Phase := 'freeing Regions';
		FreeAndNil(Regions);
		Phase := 'freeing inherited TImage';
		// inherited Free
	except
		on E: Exception do
			MessageDlg(Format('An error has occured in method TTimetableImage.Free (phase: "%s")', [Phase]), mtError, [mbIgnore], 0)
	end
end;

//

procedure TTitleImage.ClearRegions;
var i: Word;
begin
	if Regions.Count > 0 then
		for i := 0 to Regions.Count - 1 do
			Regions[i].Free;
	Regions.Clear
end;

procedure TTimetableImage.ClearRegions;
var i: Word;
begin
	if Regions.Count > 0 then
		for i := 0 to Regions.Count - 1 do
			Regions[i].Free;
	Regions.Clear
end;

//

procedure TTitleImage.RefreshTitleImageRegion(Reg: TTitleImageRegion; MouseOver: boolean);
var S: string;
begin
	with Canvas do
		begin
			Brush.Color := TitleBarColor;
			Brush.Style := bsSolid;
			FillRect(Reg.R);
			Font.Assign(FViewer.LineFontL.Font);
			S := 'Line ' + DB.Lines[Reg.Ln.Line].Name;
			if MouseOver then
				begin
					Font.Color := TScrollBox(Self.Parent).Color;
					Cursor := crHandPoint
				end
			else
				Cursor := crDefault;
			TextOut(Reg.R.Left + Reg.R.Width div 2 - TextWidth(S) div 2, Reg.R.Top + Reg.R.Height div 2 - TextHeight(S) div 2, S)
		end
end;

procedure TTitleImage.TitleImageOnMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
var i: Word;
begin
	if Regions.Count > 0 then
		for i := 0 to Regions.Count - 1 do
			begin
				if Regions[i].R.Contains(Point(X, Y)) then
					RefreshTitleImageRegion(Regions[i], True)
				else
					RefreshTitleImageRegion(Regions[i], False)
			end
end;

procedure TTitleImage.TitleImageOnMouseEnter(Sender: TObject);
begin
	TitleImageOnMouseMove(Sender, [], - 1, - 1)
end;

procedure TTitleImage.TitleImageOnMouseLeave(Sender: TObject);
begin
	TitleImageOnMouseMove(Sender, [], - 1, - 1)
end;

procedure TTitleImage.RefreshTitleImage;
var i: Integer;
begin
	Picture := nil;
	with Canvas do
		begin
			Brush.Color := TitleBarColor;
			Brush.Style := bsSolid;
			FillRect(Rect(0, 0, Width, Height))
		end;
	if Regions.Count > 0 then
		for i := 0 to Regions.Count - 1 do
			RefreshTitleImageRegion(Regions[i], False)
end;

procedure TTimetableImage.RefreshTimetableImage;
begin
	//
end;

end.
