object FAbout: TFAbout
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'About'
  ClientHeight = 330
  ClientWidth = 365
  Color = 12510427
  Constraints.MaxHeight = 720
  Constraints.MaxWidth = 365
  Constraints.MinHeight = 240
  Constraints.MinWidth = 365
  Font.Charset = EASTEUROPE_CHARSET
  Font.Color = clBlack
  Font.Height = -19
  Font.Name = 'Segoe UI'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  ScreenSnap = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 25
  object Shape1: TShape
    Left = 40
    Top = 8
    Width = 65
    Height = 65
    Brush.Style = bsClear
    Pen.Width = 2
  end
  object titleL: TLabel
    Left = 24
    Top = 16
    Width = 62
    Height = 30
    Caption = 'About'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clBlack
    Font.Height = -21
    Font.Name = 'Segoe UI'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object subtitleL: TLabel
    Left = 24
    Top = 41
    Width = 306
    Height = 15
    Caption = 'Information regarding the development of the application'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = 3426627
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel
    Left = 40
    Top = 68
    Width = 78
    Height = 21
    Caption = 'v1.0 alpha'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clBlack
    Font.Height = -16
    Font.Name = 'Segoe UI'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 136
    Top = 71
    Width = 115
    Height = 17
    Alignment = taRightJustify
    Caption = 'February 17th, 2014'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Shape2: TShape
    Left = 90
    Top = 231
    Width = 169
    Height = 57
    Visible = False
  end
  object Label3: TLabel
    Left = 136
    Top = 87
    Width = 115
    Height = 17
    Alignment = taRightJustify
    Caption = 'February 18th, 2014'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 136
    Top = 103
    Width = 115
    Height = 17
    Alignment = taRightJustify
    Caption = 'February 20th, 2014'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 137
    Top = 119
    Width = 114
    Height = 17
    Alignment = taRightJustify
    Caption = 'February 21st, 2014'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 132
    Top = 135
    Width = 119
    Height = 17
    Alignment = taRightJustify
    Caption = 'February 22nd, 2014'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Label7: TLabel
    Left = 134
    Top = 151
    Width = 117
    Height = 17
    Alignment = taRightJustify
    Caption = 'February 23rd, 2014'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Label8: TLabel
    Left = 136
    Top = 168
    Width = 115
    Height = 17
    Alignment = taRightJustify
    Caption = 'February 24th, 2014'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Label9: TLabel
    Left = 40
    Top = 188
    Width = 69
    Height = 21
    Caption = 'v1.0 beta'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clBlack
    Font.Height = -16
    Font.Name = 'Segoe UI'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label10: TLabel
    Left = 136
    Top = 191
    Width = 115
    Height = 17
    Alignment = taRightJustify
    Caption = 'February 26th, 2014'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
end
