unit StringTokenizer;

interface

uses
	Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes;

type
	TStringTokenizer = class
	private
		S: string;
	public
		constructor Create(Source: string);
		function HasMoreTokens: boolean;
		function NextToken(Delimiter: string): string; overload;
		function NextToken: string; overload;
		property GetString: string read S;
	end;

implementation

constructor TStringTokenizer.Create(Source: string);
begin
	S := Source
end;

function TStringTokenizer.HasMoreTokens: boolean;
begin
	Result := S <> ''
end;

function TStringTokenizer.NextToken(Delimiter: string): string;
begin
	if Pos(Delimiter, S) = 0 then
		Result := S
	else
		Result := Copy(S, 1, Pos(Delimiter, S) - 1);
	if HasMoreTokens then
	begin
		if Pos(Delimiter, S) = 0 then
			S := ''
		else
			Delete(S, 1, Pos(Delimiter, S) + Length(Delimiter) - 1)
	end
end;

function TStringTokenizer.NextToken: string;
begin
	Result := NextToken(' ')
end;

end.
