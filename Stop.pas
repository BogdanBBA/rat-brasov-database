unit Stop;

interface

uses
	Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
	Dialogs, Generics.Collections, Timetable;

type
	TStop = class
	private
		theName, theHREF: string;
	public
		Timetables: TList<TTimetable>;
		property Name: string read theName;
		property HREF: string read theHREF;
		constructor Create(aName, aHREF: string);
		destructor Free;
		function TotalDepartures: Integer;
		function TotalTimetables: Integer;
		function ToString: string; override;
	end;

implementation

uses DataTypes;

constructor TStop.Create(aName, aHREF: string);
begin
	theName := aName;
	theHREF := aHREF;
	Timetables := TList<TTimetable>.Create
end;

destructor TStop.Free;
begin
	FreeAndNil(Timetables)
end;

function TStop.TotalDepartures: Integer;
var N, i: Word;
begin
	N := 0;
	if Timetables.Count > 0 then
		for i := 0 to Timetables.Count - 1 do
			Inc(N, Timetables[i].TotalDepartures);
	Result := N
end;

function TStop.TotalTimetables: Integer;
begin
	Result := Timetables.Count
end;

function TStop.ToString: string;
var i: Word; S: string;
begin
	S := Format('Stop(name: %s, href: %s)%s', [theName, theHREF, NL]);
	if Timetables.Count > 0 then
		for i := 0 to Timetables.Count - 1 do
			S := Format('%s --- %s%s', [S, Timetables[i].ToString, NL]);
	Result := S
end;

end.
