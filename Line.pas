unit Line;

interface

uses
	Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
	Dialogs, Generics.Collections, Stop;

type
	TLine = class
	private
		theName, theVehicleType, theRoute: string;
		theLength: Real;
	public
		Stops: TList<TStop>;
		property Name: string read theName;
		property VehicleType: string read theVehicleType;
		property RouteDescription: string read theRoute;
		property Length: Real read theLength;
		constructor Create(aName, aVehicleType, aRoute: string; aLength: Real);
		destructor Free;
		function TotalDepartures: Integer;
		function TotalTimetables: Integer;
		function TotalStops: Integer;
		function GetStopIndex(StationName: string): Integer;
		function GetRouteForStationIndex(X: Integer; var DepS, ArrS: Integer): boolean;
		function ToString: string; override;
	end;

implementation

uses DataTypes;

constructor TLine.Create(aName, aVehicleType, aRoute: string; aLength: Real);
begin
	theName := aName;
	theVehicleType := aVehicleType;
	theRoute := aRoute;
	theLength := aLength;
	Stops := TList<TStop>.Create
end;

destructor TLine.Free;
begin
	FreeAndNil(Stops)
end;

function TLine.TotalDepartures: Integer;
var N, i: Word;
begin
	N := 0;
	if Stops.Count > 0 then
		for i := 0 to Stops.Count - 1 do
			Inc(N, Stops[i].TotalDepartures);
	Result := N
end;

function TLine.TotalTimetables: Integer;
var N, i: Word;
begin
	N := 0;
	if Stops.Count > 0 then
		for i := 0 to Stops.Count - 1 do
			Inc(N, Stops[i].TotalTimetables);
	Result := N
end;

function TLine.TotalStops: Integer;
begin
	Result := Stops.Count
end;

function TLine.GetStopIndex(StationName: string): Integer;
var i: Integer;
begin
	Result := - 1;
	if Stops.Count > 0 then
		for i := 0 to Stops.Count - 1 do
			if AnsiLowerCase(Stops[i].Name) = AnsiLowerCase(StationName) then
				begin
					Result := i;
					Break
				end
end;

function TLine.GetRouteForStationIndex(X: Integer; var DepS, ArrS: Integer): boolean;
var Phase: string; i, prevN: Integer;
	SL: TStringList; SLL: TList<TStringList>;
begin
	//
	try
		Phase := 'initializing';
		DepS := - 1;
		ArrS := - 1;
		if Stops.Count > 0 then
			if (X >= 0) and (X < Stops.Count) then
				begin
					i := 0;
					SL := TStringList.Create;
					SLL := TList<TStringList>.Create;
					while (i < Stops.Count) do
						begin
							SL.Add(Stops[i].Name);
							if Stops[i].HREF = '' then
								begin
									SLL.Add(SL);
									SL := TStringList.Create
								end;
							Inc(i)
						end;
					if SLL.Count > 0 then
						for i := 0 to SLL.Count - 1 do
							begin
								if i = 0 then
									prevN := 0
								else
									Inc(prevN, SLL[i - 1].Count);
								if (X >= prevN) and (X < prevN + SLL[i].Count) then
									begin
										DepS := prevN;
										ArrS := prevN + SLL[i].Count - 1
									end
							end
				end;
		Result := (DepS <> - 1) and (ArrS <> - 1)
	except
		on E: Exception do
			begin
				Result := False;
				MessageDlg(Format('An error has occured in method TLine.GetRouteForStationIndex(X=%d) (phase "%s"):%s%s: %s',
					[X, Phase, DNL, E.ClassName, E.Message]), mtError, [mbOK], 0)
			end
	end
end;

function TLine.ToString: string;
var i: Word; S: string;
begin
	S := Format('Line(name: %s, type: %s, length: %.2f, route: "%s")%s', [theName, theVehicleType, theLength, theRoute, NL]);
	if Stops.Count > 0 then
		for i := 0 to Stops.Count - 1 do
			S := Format('%s ** %s', [S, Stops[i].ToString]);
	Result := S
end;

end.
