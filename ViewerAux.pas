unit ViewerAux;

interface

uses
	Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
	Dialogs, Generics.Collections, Line, DepartureTime;

type
	TViewerLine = class
	public
		Line: Integer;
		DepartureStation: Integer;
		ArrivalStation: Integer;
		constructor Create; overload;
		constructor Create(aLine, aDepS, aArrS: Integer); overload;
	end;

type
	TIdentityDepartureTime = class(TDepartureTime)
	public
		Line: string;
		Station: string;
		constructor Create; overload;
		constructor Create(H, M: Byte; aLine, aStation: string); overload;
		constructor Create(DT: TDepartureTime; aLine, aStation: string); overload;
	end;

implementation

constructor TViewerLine.Create;
begin
	Line := -1;
	DepartureStation := -1;
	ArrivalStation := -1
end;

constructor TViewerLine.Create(aLine, aDepS, aArrS: Integer);
begin
	Line := aLine;
	DepartureStation := aDepS;
	ArrivalStation := aArrS
end;

constructor TIdentityDepartureTime.Create;
begin
	inherited Create(0, 0);
	Line := '';
	Station := ''
end;

constructor TIdentityDepartureTime.Create(H, M: Byte; aLine, aStation: string);
begin
	inherited Create(H, M);
	Line := aLine;
	Station := aStation
end;

constructor TIdentityDepartureTime.Create(DT: TDepartureTime; aLine, aStation: string);
begin
	Self.Create(DT.Hour, DT.Minute, aLine, aStation)
end;

end.
