unit About;

interface

uses
	Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
	Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ImageButton, DataTypes, Vcl.ExtCtrls,
	Vcl.StdCtrls;

type
	TFAbout = class(TForm)
		Shape1: TShape;
		titleL: TLabel;
		subtitleL: TLabel;
		Label1: TLabel;
		Label2: TLabel;
		Shape2: TShape;
		Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
		procedure FormCreate(Sender: TObject);
	private
		procedure CloseForm(Sender: TObject);
	public
		{ Public declarations }
	end;

var
	FAbout: TFAbout;
	CloseB: TImageButton;

implementation

{$R *.dfm}


uses Main;

procedure TFAbout.FormCreate(Sender: TObject);
begin
	FAbout.Color := FormColor;
	CloseB := TImageButton.CreateImageButton(FAbout, 'Close', FMain.buttFontL.Font, clBlack, ControlColor, ControlMOColor,
		Rect(Shape2.Left, Shape2.Top, Shape2.Width, Shape2.Height));
	CloseB.OnClick := CloseForm;
	FAbout.Height := CloseB.Top + CloseB.Height + 40;
	Shape1.SetBounds(4, 4, FAbout.Width - 8, FAbout.Height - 8)
end;

procedure TFAbout.CloseForm(Sender: TObject);
begin
	FAbout.Close
end;

end.
