object FUpdater: TFUpdater
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'Updater'
  ClientHeight = 500
  ClientWidth = 775
  Color = 12510427
  Constraints.MaxHeight = 500
  Constraints.MaxWidth = 775
  Constraints.MinHeight = 500
  Constraints.MinWidth = 775
  Font.Charset = EASTEUROPE_CHARSET
  Font.Color = clBlack
  Font.Height = -19
  Font.Name = 'Segoe UI'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  ScreenSnap = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 25
  object Shape1: TShape
    Left = 40
    Top = 8
    Width = 65
    Height = 65
    Brush.Style = bsClear
    Pen.Width = 2
  end
  object titleL: TLabel
    Left = 24
    Top = 16
    Width = 172
    Height = 30
    Caption = 'Database updater'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clBlack
    Font.Height = -21
    Font.Name = 'Segoe UI'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object subtitleL: TLabel
    Left = 24
    Top = 44
    Width = 269
    Height = 15
    Caption = 'The core feature of the application. But will it float?'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = 3426627
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel
    Left = 187
    Top = 399
    Width = 50
    Height = 21
    Caption = 'Label1'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = 9196830
    Font.Height = -16
    Font.Name = 'Segoe UI'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = False
  end
  object Label2: TLabel
    Left = 187
    Top = 422
    Width = 44
    Height = 20
    Caption = 'Label2'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = 7425828
    Font.Height = -15
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object Label3: TLabel
    Left = 187
    Top = 445
    Width = 38
    Height = 17
    Caption = 'Label3'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object Label0: TLabel
    Left = 128
    Top = 399
    Width = 53
    Height = 23
    Caption = 'Label0'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = 1117614
    Font.Height = -17
    Font.Name = 'Segoe UI'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = False
  end
  object Label4: TLabel
    Left = 267
    Top = 402
    Width = 37
    Height = 17
    Caption = 'Label4'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = 14025648
    Font.Height = -13
    Font.Name = 'Segoe UI Semilight'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object Label5: TLabel
    Left = 267
    Top = 424
    Width = 37
    Height = 17
    Caption = 'Label5'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clGreen
    Font.Height = -13
    Font.Name = 'Segoe UI Semilight'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object Label6: TLabel
    Left = 267
    Top = 447
    Width = 37
    Height = 17
    Caption = 'Label6'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = 193
    Font.Height = -13
    Font.Name = 'Segoe UI Semilight'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object Label7: TLabel
    Left = 267
    Top = 470
    Width = 38
    Height = 17
    Caption = 'Label7'
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = 1012170
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object RE: TRichEdit
    Left = 24
    Top = 77
    Width = 721
    Height = 316
    HideScrollBars = False
    Lines.Strings = (
      'RE')
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 0
  end
  object ch1: TCheckBox
    Left = 592
    Top = 16
    Width = 153
    Height = 17
    Caption = 'Stop download on error'
    Checked = True
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    State = cbChecked
    TabOrder = 1
  end
  object ch2: TCheckBox
    Left = 592
    Top = 39
    Width = 153
    Height = 17
    Caption = 'Scroll to bottom'
    Checked = True
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    State = cbChecked
    TabOrder = 2
  end
  object TestT: TTimer
    Enabled = False
    Interval = 2000
    OnTimer = TestTTimer
    Left = 248
    Top = 104
  end
  object LineIndexT: TTimer
    Enabled = False
    OnTimer = LineIndexTTimer
    Left = 312
    Top = 120
  end
  object StationListT: TTimer
    Enabled = False
    Interval = 500
    OnTimer = StationListTTimer
    Left = 376
    Top = 136
  end
  object TimetableT: TTimer
    Enabled = False
    Interval = 350
    OnTimer = TimetableTTimer
    Left = 440
    Top = 120
  end
  object FinishT: TTimer
    Enabled = False
    Interval = 500
    OnTimer = FinishTTimer
    Left = 504
    Top = 104
  end
end
