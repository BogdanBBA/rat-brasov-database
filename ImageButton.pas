unit ImageButton;

interface

uses
	Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, Vcl.Forms, Vcl.Dialogs,
	System.Classes, Vcl.Controls, Vcl.Graphics, Vcl.ExtCtrls, Vcl.StdCtrls;

type
	TImageButton = class(TImage)
	private
		aCaption: string;
		aCaptionFont: TFont;
		aBorderColor, aButtonColor, aButtonMouseOverColor: TColor;
		procedure RefreshImageButton(Color: TColor);
		procedure ImageButtonOnMouseEnter(Sender: TObject);
		procedure ImageButtonOnMouseLeave(Sender: TObject);
	public
		property Caption: string read aCaption write aCaption;
		property CaptionFont: TFont read aCaptionFont write aCaptionFont;
		property BorderColor: TColor read aBorderColor write aBorderColor;
		property ButtonColor: TColor read aButtonColor write aButtonColor;
		property ButtonMouseOverColor: TColor read aButtonMouseOverColor write aButtonMouseOverColor;
		function ToString: string; override;
		procedure RefreshIB;
		constructor CreateImageButton(aParent: TWinControl); overload;
		constructor CreateImageButton(aParent: TWinControl; pCaption: string; pCaptionFont: TFont;
			pBorderColor, pButtonColor, pButtonMouseOverColor: TColor; Bounds: TRect); overload;
		destructor FreeImageButton;
	end;

implementation

function TImageButton.ToString: string;
begin
	Result := Format('TImageButton(Caption: "%s", Font("%s" %dpt), BorderColor: %d, ButtonColor: %d, ButtonMouseOverColor: %d): Rect(%d, %d, %d, %d).',
		[Caption, CaptionFont.Name, CaptionFont.Size, BorderColor, ButtonColor, ButtonMouseOverColor, Left, Top, Width, Height])
end;

procedure TImageButton.RefreshIB;
begin
	RefreshImageButton(aButtonColor)
end;

procedure TImageButton.RefreshImageButton(Color: TColor);
var valTextWidth, valTextHeight: integer;
begin
	Picture := nil;
	with Canvas do
		begin
			Brush.Color := Color;
			Pen.Width := 1;
			Rectangle(0, 0, Width, Height);
			Font := CaptionFont;
			Brush.Style := bsClear;
			valTextWidth := TextWidth(Caption);
			valTextHeight := TextHeight(Caption);
			TextOut(Width div 2 - valTextWidth div 2, Height div 2 - valTextHeight div 2, Caption)
		end
end;

procedure TImageButton.ImageButtonOnMouseEnter(Sender: TObject);
begin
	with TImageButton(Sender) do
		RefreshImageButton(ButtonMouseOverColor)
end;

procedure TImageButton.ImageButtonOnMouseLeave(Sender: TObject);
begin
	with TImageButton(Sender) do
		RefreshImageButton(ButtonColor)
end;

//

constructor TImageButton.CreateImageButton(aParent: TWinControl);
begin
	inherited Create(aParent);
	Parent := aParent;
	Cursor := crHandPoint;
	Caption := '';
	CaptionFont := TFont.Create;
	BorderColor := clBlack;
	ButtonColor := clWhite;
	ButtonMouseOverColor := clSilver;
	SetBounds(0, 0, 100, 100);
	OnMouseEnter := ImageButtonOnMouseEnter;
	OnMouseLeave := ImageButtonOnMouseLeave;
	ImageButtonOnMouseLeave(Self)
end;

constructor TImageButton.CreateImageButton(aParent: TWinControl; pCaption: string; pCaptionFont: TFont;
	pBorderColor, pButtonColor, pButtonMouseOverColor: TColor; Bounds: TRect);
begin
	inherited Create(aParent);
	Parent := aParent;
	Cursor := crHandPoint;
	Caption := pCaption;
	CaptionFont := TFont.Create;
	CaptionFont.Assign(pCaptionFont);
	BorderColor := pBorderColor;
	ButtonColor := pButtonColor;
	ButtonMouseOverColor := pButtonMouseOverColor;
	SetBounds(Bounds.Left, Bounds.Top, Bounds.Right, Bounds.Bottom);
	OnMouseEnter := ImageButtonOnMouseEnter;
	OnMouseLeave := ImageButtonOnMouseLeave;
	ImageButtonOnMouseLeave(Self)
end;

destructor TImageButton.FreeImageButton;
begin
	try
		CaptionFont.Free;
		inherited Free
	except
		on E: Exception do
			MessageDlg(Format('TImageButton.FreeImageButton: %s - %s', [E.ClassName, E.Message]), mtError, [mbOK], 0)
	end
end;

end.
