unit TimetableImage;

interface

uses
	Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
	Vcl.ExtCtrls, Vcl.StdCtrls,
	Dialogs, Generics.Collections, ViewerAux, Line, DepartureTime;

type
	TTimetableImageRegion = class
	public
		R: TRect;
		Dep: TIdentityDepartureTime;
		constructor Create;
	end;

type
	TTimetableImageHourRegion = class
	public
		R: TRect;
		HourText: string;
	end;

type
	TTimetableImage = class(TImage)
	public
		Regions: TList<TTimetableImageRegion>;
		HRegions: TList<TTimetableImageHourRegion>;
		constructor Create(aParent: TWinControl);
		destructor Free;
		procedure ClearRegions;
		procedure TestPaint;
		procedure RefreshTimetableImageRegion(Reg: TTimetableImageRegion; MouseOver: boolean);
		procedure RefreshTimetableImageHourRegion(Reg: TTimetableImageHourRegion; MouseOver: boolean);
		procedure RefreshTimetableImage;
	end;

implementation

uses DataTypes, Viewer;

constructor TTimetableImageRegion.Create;
begin
	Dep := TIdentityDepartureTime.Create
end;

//

constructor TTimetableImage.Create(aParent: TWinControl);
begin
	inherited Create(aParent);
	Parent := aParent;
	Regions := TList<TTimetableImageRegion>.Create;
	HRegions := TList<TTimetableImageHourRegion>.Create
end;

destructor TTimetableImage.Free;
var Phase: string;
begin
	try
		Phase := 'freeing Regions';
		FreeAndNil(Regions);
		FreeAndNil(HRegions);
		Phase := 'freeing inherited TImage';
		// inherited Free
	except
		on E: Exception do
			MessageDlg(Format('An error has occured in method TTimetableImage.Free (phase: "%s")', [Phase]), mtError, [mbIgnore], 0)
	end
end;

procedure TTimetableImage.ClearRegions;
var i: Word;
begin
	if Regions.Count > 0 then
		for i := 0 to Regions.Count - 1 do
			Regions[i].Free;
	Regions.Clear
end;

//

procedure TTimetableImage.TestPaint;
var i, j: Word;
begin
	Picture := nil;
	for i := 0 to Width - 1 do
		for j := 0 to Height - 1 do
			Canvas.Pixels[i, j] := RGB(Random(j), Random(i), 32 * Random(8) - 1)
end;

procedure TTimetableImage.RefreshTimetableImageRegion(Reg: TTimetableImageRegion; MouseOver: boolean);
var S: string; R2: TRect;
begin
	with Canvas do
		begin
			Brush.Color := clWhite;
			Brush.Style := bsSolid;
			FillRect(Reg.R);
			if DB.ViewSettings.Root.Find('showDepartureLine').Attribute['value'] = '1' then
				begin
					Brush.Color := DepartureHourColor;
					R2 := Rect(Reg.R.Left, Reg.R.Top, Reg.R.Left + DepartureLineWidth, Reg.R.Bottom);
					FillRect(R2);
					S := Reg.Dep.Line;
					Font.Assign(FViewer.DepLineFontL.Font);
					if MouseOver then
						Font.Color := clYellow;
					TextOut(R2.Left + R2.Width div 2 - TextWidth(S) div 2, R2.Top + R2.Height div 2 - TextHeight(S) div 2, S)
				end;
			//
			R2 := Rect(Reg.R.Left + Integer(DB.ViewSettings.Root.Find('showDepartureLine').Attribute['value'] = '1') * DepartureLineWidth,
				Reg.R.Top, 1, Reg.R.Bottom);
			if DB.ViewSettings.Root.Find('showDepartureHour').Attribute['value'] = '1' then
				begin
					R2.Right := R2.Left + DepartureTimeFullWidth;
					S := Reg.Dep.Time
				end
			else
				begin
					R2.Right := R2.Left + DepartureTimeHalfWidth;
					S := Format(':%.2d', [Reg.Dep.Minute])
				end;
			Font.Assign(FViewer.DepFontL.Font);
			if MouseOver then
				Font.Color := clLime;
			Brush.Style := bsClear;
			TextOut(R2.Left + R2.Width div 2 - TextWidth(S) div 2, R2.Top + R2.Height div 2 - TextHeight(S) div 2, S);
			//
			Pen.Color := clBlack;
			Pen.Style := psSolid;
			Pen.Width := 1;
			Rectangle(Reg.R)
		end
end;

procedure TTimetableImage.RefreshTimetableImageHourRegion(Reg: TTimetableImageHourRegion; MouseOver: boolean);
var R2: TRect;
begin
	if DB.ViewSettings.Root.Find('showHourColumn').Attribute['value'] <> '1' then
		Exit;
	with Canvas do
		begin
			Brush.Color := TitleBarColor;
			Brush.Style := bsSolid;
			R2 := Reg.R;
			Inc(R2.Bottom, Integer(Reg = HRegions[HRegions.Count - 1]));
			FillRect(R2);
			Font.Assign(FViewer.DepHourFontL.Font);
			if MouseOver then
				Font.Color := clFuchsia;
			TextOut(Reg.R.Left + Reg.R.Width div 2 - TextWidth(Reg.HourText) div 2, Reg.R.Top + Reg.R.Height div 2 - TextHeight(Reg.HourText) div 2, Reg.HourText)
		end
end;

//

procedure TTimetableImage.RefreshTimetableImage;
var i: Integer;
begin
	Picture := nil;
	with Canvas do
		begin
			Brush.Color := TScrollBox(Self.Parent).Color;
			Brush.Style := bsSolid;
			FillRect(Rect(0, 0, Width, Height))
		end;
	if Regions.Count > 0 then
		for i := 0 to Regions.Count - 1 do
			RefreshTimetableImageRegion(Regions[i], False);
	if HRegions.Count > 0 then
		for i := 0 to HRegions.Count - 1 do
			RefreshTimetableImageHourRegion(HRegions[i], False)
end;

end.
